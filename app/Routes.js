import React from 'react';
import {Router, Scene} from 'react-native-router-flux';
import {connect, Provider} from 'react-redux';
import SplashScreen from './screens/SplashScreen';
import configureStore from './store/configureStore';
import Quran from './screens/Quran';
import Sora from './screens/Sora';
import ReadByAyaDaily from './screens/readByAyaDaily';
import ReadByAyaDefault from './screens/readByAyaDefault';
import ReadingMood from './screens/readingMood';
import readByWord from './screens/readByWord';
import NoInternet from './screens/NoInternet';
import DailyReadCompleted from './screens/DailyReadCompleted';
import WrongReading from './screens/WrongReading';
import EditAccount from './screens/EditAccount';
import ChangePassword from './screens/ChangePassword';

const store = configureStore();
const RouterWithRedux = connect()(Router);

const Routes = () => (
  <Provider store={store}>
    <RouterWithRedux>
      <Scene key="root">
        <Scene
          key="welcome"
          component={SplashScreen}
          initial
          hideNavBar={true}
        />
        <Scene key="quran" component={Quran} hideNavBar={true} />
        <Scene key="sora" component={Sora} hideNavBar={true} />
        <Scene
          key="readByAyaDaily"
          component={ReadByAyaDaily}
          hideNavBar={true}
        />
        <Scene
          key="readByAyaDefault"
          component={ReadByAyaDefault}
          hideNavBar={true}
        />
        <Scene key="readingMood" component={ReadingMood} hideNavBar={true} />
        <Scene key="readByWord" component={readByWord} hideNavBar={true} />
        <Scene key="noInternet" component={NoInternet} hideNavBar={true} />
        <Scene
          key="dailyReadCompleted"
          component={DailyReadCompleted}
          hideNavBar={true}
        />
        <Scene key="wrongReading" component={WrongReading} hideNavBar={true} />
        <Scene key="editAccount" component={EditAccount} hideNavBar={true} />
        <Scene
          key="changePassword"
          component={ChangePassword}
          hideNavBar={true}
        />
      </Scene>
    </RouterWithRedux>
  </Provider>
);
export default Routes;
