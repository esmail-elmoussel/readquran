import React from 'react';
import {StyleSheet, FlatList, I18nManager} from 'react-native';
import {View, Text} from 'native-base';
import Header from '../components/shared/MinHeader';
import connect from '../connectedComponent/index';
import Aya from '../components/sora/ayaList';
import SoraDetails from '../components/quranView/soraDetails';
import SoraIcon from '../components/quranView/soraICon';
import * as Progress from 'react-native-progress';
import Input from '../components/sora/searchInput';
import Mic from '../components/shared/micButton';
import Loading from '../components/shared/Loading';
import Layout from '../components/shared/Layout';
import postService from '../axios/postService';
import Spinner from 'react-native-loading-spinner-overlay';
import {Actions} from 'react-native-router-flux';

class Sora extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      language: this.props.language,
      searchLoading: false,
      searchResult: [],
      noResult: false,
    };
  }

  onSearch = async data => {
    await this.setState({searchLoading: true});
    if (data <= this.props.selectedItem.number_of_ayat) {
      const newData = await this.search(data);
      this.setState({searchResult: newData});
    } else {
      this.setState({noResult: true});
      setTimeout(() => {
        this.setState({noResult: false});
      }, 250);
    }
    this.setState({searchLoading: false});
  };

  search = async data => {
    return this.props.ayat.filter(
      item => item.ayah_number === JSON.parse(data),
    );
  };

  renderItem = ({item}) => {
    return <Aya aya={item} sora={this.props.selectedItem} />;
  };

  getSpeechResult = async data => {
    const translate = this.props?.language?.text;

    if (!data.userText) {
      // eslint-disable-next-line no-alert
      alert(translate.No_data_Found_Try_Again);
    } else {
      const requestData = {
        sora_id: this.props.selectedItem.sora_number,
        sound: data.base64,
      };
      // console.log(requestData);
      this.setState({searchLoading: true});
      await postService('user/search-ayah-by-voice', requestData)
        .then(res => {
          if (res.data?.errorMessage) {
            if (
              JSON.parse(res?.data?.errorMessage)?.body ===
              'Not found in this surah'
            ) {
              this.setState({noResult: true});
              setTimeout(() => {
                this.setState({noResult: false});
              }, 250);
            }
          } else {
            const newData = [];
            newData.push(res.data);
            if (newData.length === 0) {
              this.setState({noResult: true});
              setTimeout(() => {
                this.setState({noResult: false});
              }, 250);
            }
            this.setState({searchResult: newData});
          }
          this.setState({searchLoading: false});
        })
        .catch(err => {
          console.log('sa: ', err.message);
          this.setState({searchLoading: false});
          Actions.noInternet({error: true});
        });
    }
  };

  render() {
    const translate = this.props?.language?.text;

    return this.props.loading ? (
      <Loading />
    ) : (
      <Layout>
        <Header
          title={
            this.props.mode === 'word'
              ? translate.Word_by_word
              : translate.Aya_by_Aya
          }
          showTitle={true}
          mainTitle={true}
        />
        <View style={styles.progress}>
          <Progress.Bar
            progress={
              this.props.selectedItem.user_ayat /
              this.props.selectedItem.number_of_ayat
            }
            width={325}
            height={10}
            color="#EFC91A"
            borderColor="transparent"
            unfilledColor="#ECEAEA"
          />
        </View>
        <View style={styles.View}>
          {!I18nManager.isRTL ? (
            <SoraIcon soraPlace={this.props.selectedItem.place} />
          ) : null}
          <SoraDetails sora={this.props.selectedItem} />
        </View>
        <View style={styles.search}>
          <Input
            getValue={this.onSearch}
            placeholder={translate.Write_Ayah_Number}
          />
          <Mic getSpeechResult={this.getSpeechResult} squareStyle={true} />
        </View>
        {this.state.noResult ? (
          <Text allowFontScaling={false} style={styles.text}>
            {translate.No_Result}
          </Text>
        ) : this.state.searchResult.length > 0 ? (
          <FlatList
            data={this.state.searchResult}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index}
          />
        ) : (
          <FlatList
            data={this.props.ayat}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index}
          />
        )}
        <Spinner visible={this.state.searchLoading} />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  View: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    width: '94%',
    alignSelf: 'center',
    paddingBottom: 15,
    borderBottomColor: '#DBDBDB',
    alignItems: 'center',
    borderBottomWidth: 1,
  },
  progress: {
    alignSelf: 'center',
    marginVertical: 20,
  },
  search: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  text: {
    fontSize: 15,
    fontFamily: 'ElMessiri-Regular',
    textTransform: 'capitalize',
    lineHeight: 44,
    alignSelf: 'center',
    paddingVertical: 10,
    color: '#444444',
  },
});

export default connect(Sora);
