import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  questions: [],
  title: '',
  loading: false,
  errorMessage: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    // focus action is dispatched when a new screen comes into focus
    case actionTypes.FETCH_SHORT_SURAH_QUESTION_TRIGGER:
      return {...state, loading: action.loading, questions: []};
    case actionTypes.FETCH_SHORT_SURAH_QUESTION_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        questions: action.payload,
        loading: action.loading,
      };
    case actionTypes.FETCH_SHORT_SURAH_QUESTION_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default: {
      return {
        ...state,
      };
    }
  }
}
