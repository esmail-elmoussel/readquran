import * as actionTypes from './actionTypes';

const updateLanguage = language => {
  // console.log(language);
  return async (dispatch, getState) => {
    dispatch({
      type: actionTypes.UPDATE_LANGUAGE,
      language,
    });
  };
};

export default updateLanguage;
