import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {View} from 'native-base';
import connect from '../connectedComponent/index';
import * as Animatable from 'react-native-animatable';
import {getStorage, setStorage} from '../services/localStorageManager';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import Layout from '../components/shared/Layout';

class SplashScreen extends Component {
  componentDidMount = async () => {
    const sdata = await getStorage('data');
    if (sdata?.token) {
      const {language} = sdata?.user;
      this.props.updateLanguage(language);
    } else {
      await DeviceInfo.getAndroidId().then(async androidId => {
        await this.registerNewUser(androidId);
      });
    }
    await this.props.updateUserData();
    await this.props.fetchRings();
    await this.props.fetchSuar();
    const newDate = new Date().toDateString();
    const date = await getStorage('date');
    let userCurrentPage = await getStorage('userCurrentPage');
    if (date !== newDate) {
      setStorage({
        key: 'userCurrentPageDone',
        data: false,
      });
      const data = newDate;
      await setStorage({key: 'date', data});
    } else {
      const userCurrentPageDone = await getStorage('userCurrentPageDone');
      if (userCurrentPageDone) {
        return Actions.quran();
      }
    }
    console.log('page', userCurrentPage);
    await axios
      .get(`http://api.alquran.cloud/v1/page/${userCurrentPage || 1}/en.asad`)
      .then(res => {
        const {ayahs} = res.data?.data;
        Actions.readByAyaDaily({ayahs});
      })
      .catch(err => console.log(err.message));
  };

  registerNewUser = async androidId => {
    const requestData = {
      email: `${androidId}@guest.com`,
      username: `guest${androidId}`,
      password: '12345678',
    };
    await setStorage({
      key: 'userCurrentPage',
      data: 1,
    });
    await axios
      .post(
        'https://7astc125f5.execute-api.us-east-1.amazonaws.com/Dev_template/register',
        requestData,
      )
      .then(async res => {
        console.log('register new user response :', res.data);
        await this.signinUser(androidId);
      })
      .catch(async err => {
        console.log('register new user error :', err);
        if (err.message === 'Network Error') {
          Actions.noInternet();
        } else if (
          err?.response?.data?.errorMessage?.includes('ER_DUP_ENTRY')
        ) {
          await this.signinUser(androidId);
        }
      });
  };

  signinUser = async androidId => {
    const requestData = {
      email: `${androidId}@guest.com`,
      password: '12345678',
    };

    await axios
      .post(
        'https://7astc125f5.execute-api.us-east-1.amazonaws.com/Dev_template/authenticate',
        requestData,
      )
      .then(async res => {
        console.log('signin user response :', res.data);
        const data = res.data;
        await setStorage({key: 'data', data});
        const language = data?.user?.language;
        this.props.updateLanguage(language);
      })
      .catch(err => {
        console.log('signin user error :', err);
        if (err.message === 'Network Error') {
          Actions.noInternet();
        }
      });
  };

  render() {
    const translate = this.props?.language?.text;

    return (
      <Layout>
        <View style={styles.content}>
          {/* <Image
            source={require('../assets/img/newLogo.png')}
            style={styles.logo}
            /> 
            <Text allowFontScaling={false} style={styles.title}>
            {translate.Imam}
          </Text> */}
          <Animatable.Text
            allowFontScaling={false}
            animation="slideInUp"
            iterationCount={1}
            style={styles.text}
            direction="alternate">
            Let's Read Quran
          </Animatable.Text>
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 70,
    height: 80,
  },
  title: {
    color: '#FFFFFF',
    fontSize: 35,
    alignSelf: 'center',
    fontFamily: 'ElMessiriBold',
    lineHeight: 55,
    marginTop: 10,
  },
  text: {
    color: '#707070',
    fontSize: 20,
    alignSelf: 'center',
    fontFamily: 'ElMessiri-Regular',
    fontWeight: '100',
    lineHeight: 26,
  },
});

export default connect(SplashScreen);
