import React from 'react';
import {StyleSheet, FlatList} from 'react-native';
import {Text, View} from 'native-base';
import Header from '../components/shared/MinHeader';
import connect from '../connectedComponent/index';
import WrongAya from '../components/meView/WrongAya';
import LChart from '../components/meView/lineChart';
import Layout from '../components/shared/Layout';
import Spinner from 'react-native-loading-spinner-overlay';

class WrongReading extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
    };
  }

  componentDidMount = async () => {
    if (!this.props.userWrongAyat.length) {
      await this.props.fetchWrongAyat();
    }
    this.setState({loading: false});
    this.props.fetchWrongAyat();
  };

  renderItem = ({item}) => {
    return <WrongAya aya={item} />;
  };

  render() {
    const {userWrongAyat} = this.props;
    const {loading} = this.state;
    const translate = this.props?.language?.text;

    return (
      <Layout>
        <Header
          title={translate.Ayat_Reading}
          showTitle={true}
          mainTitle={false}
        />
        <View style={styles.container}>
          <Text style={styles.paragraph}>
            {translate.Here_you_can_check_ayat_you_read_it_wrong_Weekly}
          </Text>
          <LChart withBackground={false} />
          {!userWrongAyat.length && !loading ? (
            <Text style={styles.text}>
              {translate.You_dont_have_any_wrong_ayat_yet}
            </Text>
          ) : (
            <FlatList
              data={userWrongAyat}
              renderItem={this.renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          )}
        </View>
        <Spinner visible={loading} />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '95%',
    alignSelf: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  paragraph: {
    textAlign: 'center',
    fontFamily: 'CairoRegular',
    fontSize: 14,
    color: '#707070',
    marginHorizontal: 30,
    marginTop: 30,
  },
  text: {
    flex: 1,
    lineHeight: 27,
    fontWeight: 'bold',
    color: '#707070',
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 140,
  },
});

export default connect(WrongReading);
