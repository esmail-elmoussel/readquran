import * as firebase from 'firebase';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBY5puv-fHmsoL_ZLTZ1lezPrq1_rC0MJw',
  authDomain: 'imam-210a2.firebaseapp.com',
  databaseURL: 'https://imam-210a2.firebaseio.com',
  projectId: 'imam-210a2',
  storageBucket: 'imam-210a2.appspot.com',
  messagingSenderId: '72090899389',
  appId: '1:72090899389:web:cb1934573e46d037e6f8d4',
  measurementId: 'G-9QESGKZC8L',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// export const db = firebase.firestore();
export default firebase;
export const ref = firebase.database().ref();
export const db = firebase.firestore();
