import PushNotification from 'react-native-push-notification';
import {Platform} from 'react-native';

const getDate = dayNumber => {
  let date = new Date();
  console.log('now: ', date);
  let dayToSet = dayNumber; // 0 === sunday ---> 6 === saturday

  let currentDay = date.getDay();
  let distance = (dayToSet + 7 - currentDay) % 7;
  date.setDate(date.getDate() + distance);
  date.setHours(15, 0, 0, 0);
  return date;
};

export const dayWeeklyNotifications = dayNumber => {
  console.log('reminder Notifications: ', getDate(dayNumber));
  PushNotification.configure({
    onNotification: function(notification) {
      console.log('LOCAL NOTIFICATION ==>', notification);
    },
    popInitialNotification: true,
    requestPermissions: Platform.OS === 'ios',
  });
  PushNotification.localNotificationSchedule({
    id: dayNumber,
    autoCancel: true,
    bigText:
      'Remember that learning Quran requires a little bit of practice every day.',
    title: 'It is time to learn Quran',
    message: 'Get More Deeds',
    vibrate: true,
    vibration: 300,
    playSound: true,
    soundName: 'default',
    smallIcon: 'ic_notification',
    date: getDate(dayNumber),
    repeatType: 'week',
  });
};

export const inActiveUserNotification = () => {
  console.log(
    'inActiveUserNotifications: ',
    new Date(Date.now() + 24 * 60 * 60 * 1000),
  );
  PushNotification.configure({
    // (required) Called when a remote or local notification is opened or received
    onNotification: function(notification) {
      console.log('LOCAL NOTIFICATION ==>', notification);
    },
    // foreground: false,
    popInitialNotification: true,
    requestPermissions: Platform.OS === 'ios',
  });
  PushNotification.localNotificationSchedule({
    id: 10,
    autoCancel: true,
    bigText:
      'Remember that learning Quran requires a little bit of practice every day.',
    title: 'It is time to learn Quran',
    message: 'Get More Deeds',
    vibrate: true,
    vibration: 300,
    playSound: true,
    soundName: 'default',
    smallIcon: 'ic_notification',
    // actions: '["Yes", "No"]',
    date: new Date(Date.now() + 24 * 60 * 60 * 1000),
    repeatType: 'day',
  });
};

export const cancelDayNotification = dayNumber => {
  PushNotification.cancelLocalNotifications({id: dayNumber});
};

export const cancelInActiveNotification = () => {
  console.log('cancel inActive notifications');
  PushNotification.cancelLocalNotifications({id: 10});
};

export const cancelAllNotifications = () => {
  PushNotification.cancelAllLocalNotifications();
};
