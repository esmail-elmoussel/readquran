/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View} from 'native-base';
import {StyleSheet, Image, TouchableOpacity} from 'react-native';
import connect from '../../connectedComponent/index';

class Message extends Component {
  render() {
    const translate = this.props.language?.text;
    const {isRight, show, getNextItem} = this.props;
    return show ? (
      <View
        style={[
          styles.container,
          isRight ? styles.rightAnswerContainer : null,
          this.props?.quizQuestion?.question_number === 1 &&
          this.props.step === 1
            ? styles.containerWithlargeContent
            : styles.containerWithSmallContent,
        ]}>
        <Text style={styles.text}>
          {isRight ? "you'r right" : "you'r wrong"}
        </Text>
        <TouchableOpacity
          style={styles.nextButton}
          onPress={() => getNextItem()}>
          <View>
            <Image
              source={require('../../assets/img/checkEnabled.png')}
              style={styles.buttonImage}
            />
            <Text style={styles.buttonText}>{translate.Next}</Text>
          </View>
        </TouchableOpacity>
      </View>
    ) : (
      <></>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#A00306',
    height: 125,
    marginRight: 12,
    marginLeft: 16,
    borderRadius: 5,
    width: '88%',
    alignSelf: 'center',
  },
  containerWithlargeContent: {
    marginBottom: 20,
    marginTop: '5%',
  },
  containerWithSmallContent: {
    position: 'absolute',
    bottom: '3%',
  },
  rightAnswerContainer: {
    backgroundColor: '#323F1E',
  },
  text: {
    fontFamily: 'aldhabi',
    color: 'white',
    alignSelf: 'center',
    fontSize: 40,
  },
  nextButton: {
    width: 130,
    height: 60,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  buttonImage: {
    width: 130,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  buttonText: {
    fontFamily: 'aldhabi',
    top: 45,
    position: 'absolute',
    alignSelf: 'center',
    color: 'white',
    fontSize: 45,
  },
});

export default connect(Message);
