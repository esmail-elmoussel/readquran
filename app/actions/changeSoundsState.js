import * as actionTypes from './actionTypes';
import {setStorage} from '../services/localStorageManager';

const changeSoundsState = event => {
  const data = {
    enabled: event,
  };
  setStorage({key: 'soundsEnabled', data});
  return dispatch => {
    dispatch({type: actionTypes.CHANGE_SOUNDS_STATE, payload: event});
  };
};

export default changeSoundsState;
