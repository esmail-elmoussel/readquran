import React, {Component} from 'react';
import {StyleSheet, Text, Image} from 'react-native';
import {View} from 'native-base';
import Layout from '../components/shared/Layout';
import {Actions} from 'react-native-router-flux';
import connect from '../connectedComponent/index';

class NoInternet extends Component {
  componentDidMount = () => {
    setTimeout(() => {
      Actions.pop();
    }, 3000);
  };

  render() {
    const translate = this.props?.language?.text;
    const {error} = this.props;

    return (
      <Layout>
        <View style={styles.container}>
          <Image source={require('../assets/img/imamSad.png')} />
          {error ? (
            <Text style={styles.text}>{translate.Something_went_wrong}</Text>
          ) : (
            <Text style={styles.text}>{translate.No_Internet_Connection}</Text>
          )}
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'Amiri',
    fontSize: 24,
    color: '#707070',
    marginTop: 30,
  },
});

export default connect(NoInternet);
