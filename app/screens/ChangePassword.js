import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Content, Text} from 'native-base';
import Header from '../components/shared/MinHeader';
import connect from '../connectedComponent';
import Layout from '../components/shared/Layout';
import ImamButton from '../components/shared/imamButton';
import ImamInput from '../components/shared/imamInput';
import {Actions} from 'react-native-router-flux';
import postService from '../axios/postService';
import {setStorage, getStorage} from '../services/localStorageManager';
import Toaster, {ToastStyles} from 'react-native-toaster';

class ChangePassword extends Component {
  constructor() {
    super();
    this.state = {
      oldPassword: '',
      newPassword: '',
      oldPassError: '',
      newPassError: '',
      loading: false,
      message: null,
    };
  }

  getOldPassword = data => {
    this.setState({
      oldPassword: data.value,
      oldPassError: data.error,
      disabled: true,
    });
  };

  getNewPassword = data => {
    this.setState({
      newPassword: data.value,
      newPassError: data.error,
      disabled: true,
    });
  };

  updateUserInfo = async () => {
    const {oldPassword, newPassword} = this.state;

    this.setState({loading: true});
    const user_id = this.props?.userData?.user?.id;
    const requestData = {
      user_id,
      old_password: oldPassword || '12345678',
      new_password: newPassword,
    };

    postService('user/update-user', requestData)
      .then(async res => {
        console.log('res: ', res.data);
        let message = '';
        if (res.data.errorMessage) {
          message = {
            text: res.data.errorMessage,
            styles: ToastStyles.error,
          };

          this.setState({loading: false, message});
        } else {
          const data = {
            token: {...(await getStorage('data'))}.token,
            user: res.data,
          };
          await setStorage({key: 'data', data});
          await this.props.updateUserData();
          this.setState({loading: false});
          Actions.pop();
        }
      })
      .catch(err => {
        console.log('err: ', err.response.data);
        let message = '';

        if (err.response?.data?.body) {
          message = {
            text: err.response.data.body,
            styles: ToastStyles.error,
          };
          this.setState({message});
        } else if (err.message === 'Network Error') {
          Actions.noInternet();
        } else {
          Actions.noInternet({error: true});
        }
        this.setState({loading: false});
      });
  };

  render() {
    const translate = this.props?.language?.text;
    const {loading, disabled, newPassError, message, newPassword} = this.state;

    return (
      <Layout>
        <Header title={translate.Change_password} showTitle={true} />
        <Content style={styles.content}>
          <ImamInput
            label={translate.Old_password}
            placeholder={translate.Type_your_old_password}
            errorMesssage={translate.at_least_8_characters}
            getValueAndError={this.getOldPassword}
            type={'password'}
          />
          <Text style={styles.warningText}>
            Important: If it is your first time leave it empty.
          </Text>
          <ImamInput
            label={translate.New_password}
            placeholder={translate.Type_your_new_password}
            errorMesssage={translate.at_least_8_characters}
            getValueAndError={this.getNewPassword}
            type={'password'}
          />
          <ImamButton
            loading={loading}
            disabled={disabled && !newPassError && newPassword}
            buttonText={translate.Save}
            onPress={this.updateUserInfo}
          />
        </Content>
        {message ? <Toaster message={message} duration={500} /> : null}
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    width: '93%',
    alignSelf: 'center',
    marginTop: 100,
  },
  warningText: {
    marginHorizontal: 30,
    fontSize: 13,
    marginTop: -15,
    marginBottom: 30,
    color: '#707070',
  },
});

export default connect(ChangePassword);
