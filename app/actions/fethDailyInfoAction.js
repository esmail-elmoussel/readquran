import getService from '../axios/getService';
import * as actionTypes from './actionTypes';

const fetchDailyInfo = () => {
  return async (dispatch, getState) => {
    dispatch({type: actionTypes.FETCH_DAILY_AYA_TRIGGER, loading: true});

    return await getService('user/get-daily-info')
      .then(response => {
        dispatch({
          type: actionTypes.FETCH_DAILY_AYA_SUCCESS,
          payload: response,
          loading: false,
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: actionTypes.FETCH_DAILY_AYA_FAIL,
          payload: error.message,
          loading: false,
        });
      });
  };
};

export default fetchDailyInfo;
