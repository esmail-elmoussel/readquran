import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  ayat: [],
  loading: false,
  errorMessage: null,
};

const fetchAyaReasonReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_AYA_REASONS_TRIGGER:
      return {...state, loading: action.loading, ayat: []};
    case actionTypes.GET_AYA_REASONS_SUCESS:
      return {
        ...state,
        ayat: action.payload,
        loading: action.loading,
      };
    case actionTypes.GET_AYA_REASONS_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default:
      return state;
  }
};

export default fetchAyaReasonReducer;
