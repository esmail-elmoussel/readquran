import updateLanguage from '../actions/updateLanguageAcion';
import updateUserData from '../actions/updateUserDataAction';
// import fetchShortSoraquestions from '../actions/shortSoraQuestionsAction';
import fetchSurah from '../actions/fetchSurahAction';
import fetchAyah from '../actions/fetchAyahAction';
// import fetchDailyInfo from '../actions/fethDailyInfoAction';
import fetchRings from '../actions/fetchRingsAction';
import fetchSuar from '../actions/fetchSuarAction';
import fetchHokm from '../actions/fetchAhkamAction';
import fetchWrongAyat from '../actions/fetchWrongAyatAction';
import changeAudioState from '../actions/changeAudioState';
import fetchAyaReason from '../actions/fetchAyaReason';
// import fetchRecitQuiz from '../actions/fetchRecitQuiz';
// import changeSoundsState from '../actions/changeSoundsState';
import fetchProfileImage from '../actions/fetchProfileImage';
// import reminderAction from '../actions/reminderAction';

function mapDispatchToProps(dispatch) {
  return {
    updateLanguage: Key => dispatch(updateLanguage(Key)),
    updateUserData: () => dispatch(updateUserData()),
    // fetchShortSoraquestions: data => dispatch(fetchShortSoraquestions(data)),
    fetchSurah: Key => dispatch(fetchSurah(Key)),
    fetchHokm: key => dispatch(fetchHokm(key)),
    fetchSuar: () => dispatch(fetchSuar()),
    fetchAyah: data => dispatch(fetchAyah(data)),
    // fetchDailyInfo: Key => dispatch(fetchDailyInfo(Key)),
    fetchRings: () => dispatch(fetchRings()),
    fetchWrongAyat: () => dispatch(fetchWrongAyat()),
    changeAudioState: event => dispatch(changeAudioState(event)),
    fetchAyaReason: data => dispatch(fetchAyaReason(data)),
    // fetchRecitQuiz: data => dispatch(fetchRecitQuiz(data)),
    // changeSoundsState: event => dispatch(changeSoundsState(event)),
    fetchProfileImage: () => dispatch(fetchProfileImage()),
    // reminderAction: (day, event) => dispatch(reminderAction(day, event)),
  };
}

export default mapDispatchToProps;
