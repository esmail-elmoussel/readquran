/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Buffer} from 'buffer';
import AudioRecord from 'react-native-audio-record';
import {checkPermission} from '../../config/permission';
import {getFileInBase64} from '../../services/recordService';
import Sound from 'react-native-sound';
import StopIcon from 'react-native-vector-icons/Entypo';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import {Wander} from 'react-native-animated-spinkit';
import connect from '../../connectedComponent';

class MicButton extends Component {
  state = {
    recording: false,
    loading: false,
  };

  async componentDidMount() {
    await checkPermission();

    const options = {
      sampleRate: 16000,
      channels: 1,
      bitsPerSample: 16,
      wavFile: 'test.wav',
    };

    AudioRecord.init(options);

    AudioRecord.on('data', data => {
      const chunk = Buffer.from(data, 'base64');
      console.log('chunk size', chunk.byteLength);
      // do something with audio chunk
    });
  }

  start = () => {
    console.log('start record');
    this.setState({audioFile: '', recording: true, loaded: false});
    AudioRecord.start();
  };

  stop = async () => {
    if (!this.state.recording) {
      return;
    }
    console.log('stop record');
    let audioFile = await AudioRecord.stop();
    const base64 = await getFileInBase64({fileLocation: audioFile});
    this.setState({recording: false});
    await this.convertToText(base64);
  };

  convertToText = async base64 => {
    this.setState({loading: true});
    const data = {base64};
    await axios
      .post(
        'https://7astc125f5.execute-api.us-east-1.amazonaws.com/dev/speech-to-text',
        data,
      )
      .then(res => {
        console.log('response', res.data.text);
        const resData = {
          userText: res.data.text,
          base64,
        };
        this.props.getSpeechResult(resData);
      })
      .catch(error => {
        console.log(error);
        if (error.message === 'Network Error') {
          Actions.noInternet();
        } else {
          Actions.noInternet({error: true});
        }
      });
    this.setState({loading: false});
  };

  playTone = async () => {
    if (this.props.soundsEnabled) {
      let callback = (error, sound) => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        sound.play(() => {
          sound.release();
        });
      };
      var sound = new Sound('soft_notification.mp3', Sound.MAIN_BUNDLE, error =>
        callback(error, sound),
      );
    }
  };

  recordSound = async () => {
    this.playTone();
    if (this.pressed) {
      this.pressed = false;
      await this.stop();
    } else {
      this.pressed = true;
      await this.start();
    }
  };

  componentWillUnmount = () => {
    if (this.state.audioFile) {
      this.stop();
    }
  };

  render() {
    const {loading, recording} = this.state;

    return (
      <TouchableHighlight
        style={[
          !this.props.squareStyle ? styles.highlight : styles.squareStyle,
          loading ? styles.loading : null,
          this.props.fromMap && !loading ? styles.mapHighlight : null,
        ]}>
        <TouchableOpacity
          disabled={loading}
          onPress={async () => {
            this.recordSound();
            this.props?.micPressed();
          }}>
          <Image
            source={require('../../assets/img/mic.png')}
            style={[
              styles.image,
              {display: recording || loading ? 'none' : 'flex'},
            ]}
          />
          <StopIcon
            name={'controller-stop'}
            size={25}
            color={'white'}
            style={[styles.icon, {display: recording ? 'flex' : 'none'}]}
          />
          <Wander
            size={25}
            color="#E59D5C"
            style={[styles.icon, {display: loading ? 'flex' : 'none'}]}
          />
        </TouchableOpacity>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  highlight: {
    borderRadius: 72 / 2,
    justifyContent: 'center',
    width: 72,
    height: 72,
    alignSelf: 'center',
    backgroundColor: '#478D73',
    borderColor: '#478D73',
    borderWidth: 1,
    borderBottomWidth: 3,
  },
  mapHighlight: {
    backgroundColor: '#323F1E',
    borderColor: '#323F1E',
  },
  squareStyle: {
    width: 50,
    height: 50,
    borderRadius: 11,
    backgroundColor: '#478D73',
    alignSelf: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#326351',
    borderBottomWidth: 3,
  },
  icon: {
    alignSelf: 'center',
  },
  image: {
    width: 25,
    height: 25,
    alignSelf: 'center',
  },
  loading: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
  },
});

export default connect(MicButton);
