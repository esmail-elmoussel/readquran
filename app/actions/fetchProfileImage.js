import * as actionTypes from './actionTypes';
import {getStorage} from '../services/localStorageManager';

const fetchProfileImage = () => {
  return async (dispatch, getState) => {
    const user = {...(await getStorage('data'))}.user;
    const image = await getStorage('image');
    let imageUrl;
    if (user.image) {
      imageUrl = {
        uri:
          'https://imam-bucket.s3.amazonaws.com/' +
          user.image +
          '?time' +
          new Date().getTime(),
        headers: {Pragma: 'no-cache'},
      };
    } else if (user.facebook_id && user.image === null && image) {
      imageUrl = {uri: image};
    } else {
      imageUrl =
        user.gender === 'male'
          ? require('../assets/img/maleProfile.png')
          : require('../assets/img/femaleProfile.png');
    }
    dispatch({
      type: actionTypes.FETCH_IMAGE,
      url: imageUrl,
    });
  };
};

export default fetchProfileImage;
