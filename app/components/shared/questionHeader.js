import React, {Component} from 'react';
import {StyleSheet, BackHandler, Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import MapPopup from '../map/Popup';
import {clearItem} from '../../services/localStorageManager';
import connect from '../../connectedComponent/index';

class questionHeader extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
    };
  }

  goBack = async () => {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );

    this.toggleModal();
    if (this.props.map) {
      Actions.map();
      await clearItem('total');
    } else {
      Actions.pop();
    }
  };

  toggleModal = () => {
    const {modalVisible} = this.state;
    this.setState({modalVisible: !modalVisible});
  };

  componentDidMount = () => {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  };

  handleBackButtonClick = () => {
    this.setState(state => {
      return {
        modalVisible: !state.modalVisible,
      };
    });
    return true;
  };

  render() {
    const {modalVisible} = this.state;
    const {headerTitle, language} = this.props;
    const translate = language?.text;

    return (
      <>
        <Text style={styles.title} allowFontScaling={false}>
          {headerTitle}
        </Text>
        <MapPopup
          toggleModal={this.toggleModal}
          visible={modalVisible}
          subTitle={translate.Are_you_sure}
          text={translate.If_you_closed_now_your_progress_will_be_erased}
          firstButtonText={translate.Cancel}
          secondButtonText={translate.Close}
          onFirstButtonPress={this.toggleModal}
          onSecondButtonPress={this.goBack}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#FFFFFF',
    alignSelf: 'center',
    fontFamily: 'aldhabi',
    fontSize: 40,
    marginTop: -7,
  },
});

export default connect(questionHeader);
