import postService from './postService';
import {getStorage} from '../services/localStorageManager';

const postByUserId = async route => {
  const data = await getStorage('data');
  const requestData = {
    user_id: data.user.id,
  };
  return postService(route, requestData);
};

export default postByUserId;
