/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Input, View, Label, Item} from 'native-base';
import InputIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import UserIcon from 'react-native-vector-icons/Feather';

class ImamInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      onFocus: this.props.onFocus,
      value: '',
      error: false,
      hasError: false,
      showPassword: false,
    };
  }

  onchangeValue = async value => {
    await this.setState({value});
    if (
      (this.props.type === 'password' && value.length < 8) ||
      (this.props.type === 'email' && !this.validate(value)) ||
      (this.props.type === 'text' && value.length === 0) ||
      (this.props.label === 'Confirm Password' && !this.mustMutch(value))
    ) {
      await this.setState({error: true});
    } else {
      await this.setState({error: false});
    }
    const data = {
      value: this.state.value,
      error: this.state.error,
    };
    this.props.getValueAndError(data);
  };

  onBlur = () => {
    if (this.state.error === true) {
      this.setState({onFocus: false, hasError: true});
    } else {
      this.setState({onFocus: false, hasError: false});
    }
  };

  mustMutch = value => {
    if (value === this.props.pass) {
      return true;
    } else {
      return false;
    }
  };

  validate = email => {
    const emailVal = email.replace(' ', '');
    const expression = /.+@.+\.[A-Za-z]+$/;

    return expression.test(String(emailVal).toLowerCase());
  };

  renderIcon = () => {
    if (this.props.type === 'email') {
      return <InputIcon name={'email-outline'} size={20} color={'#B5B5B5'} />;
    } else if (this.props.type === 'password') {
      return (
        <TouchableOpacity
          onPress={() => {
            requestAnimationFrame(() => {
              this.setState(state => {
                return {
                  showPassword: !state.showPassword,
                };
              });
            });
          }}>
          <InputIcon
            name={this.state.showPassword ? 'eye-off' : 'eye'}
            size={20}
            color={'#B5B5B5'}
          />
        </TouchableOpacity>
      );
    } else if (this.props.type === 'text') {
      return (
        <UserIcon
          name={'user'}
          size={20}
          color={'#B5B5B5'}
          style={{
            display: this.props.label === 'Referral code' ? 'none' : 'flex',
          }}
        />
      );
    }
  };

  render() {
    const {showPassword, onFocus, hasError, value, error} = this.state;

    return (
      <View style={styles.itemView}>
        <Label style={styles.Label} allowFontScaling={false}>
          {this.props.label}
        </Label>
        <Item
          style={[
            styles.item,
            {
              borderColor: onFocus
                ? '#C9AD28'
                : hasError
                ? '#CC5148'
                : '#C4C1C1',
            },
          ]}
          rounded>
          {this.renderIcon()}
          <Input
            onFocus={() => {
              this.setState({onFocus: true});
            }}
            onBlur={() => {
              this.onBlur();
            }}
            placeholder={this.props.placeholder}
            secureTextEntry={this.props.type === 'password' && !showPassword}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            autoCompleteType={'off'}
            onChangeText={v => {
              this.onchangeValue(v);
            }}
            value={value}
            placeholderTextColor="#B5B5B5"
            allowFontScaling={false}
          />
        </Item>
        <Label style={styles.error} allowFontScaling={false}>
          {error ? `${this.props.errorMesssage}` : ''}
        </Label>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#FAFAFA',
    alignSelf: 'center',
    marginTop: 8,
    width: 300,
    height: 46,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 15,
    borderColor: '#C4C1C1',
  },
  itemView: {
    marginBottom: 15,
    alignSelf: 'center',
  },
  input: {
    color: '#51565F',
    fontSize: 12,
    fontFamily: 'CairoRegular',
  },
  Label: {
    fontFamily: 'CairoRegular',
    paddingLeft: 15,
    fontSize: 12,
    color: '#51565F',
  },
  error: {
    fontFamily: 'CairoRegular',
    paddingLeft: 15,
    fontSize: 10,
    color: 'red',
    marginTop: 1,
  },
});

export default ImamInput;
