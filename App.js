import React, {Component} from 'react';
import {
  Platform,
  InteractionManager,
  BackHandler,
  AppState,
  StatusBar,
  I18nManager,
} from 'react-native';
import Routes from './app/Routes';
import {MenuProvider} from 'react-native-popup-menu';
import {Actions} from 'react-native-router-flux';
import {
  inActiveUserNotification,
  cancelInActiveNotification,
} from './app/services/notificationService';
import connect from './app/connectedComponent/index';
import {getStorage} from './app/services/localStorageManager';

const _setTimeout = global.setTimeout;
const _clearTimeout = global.clearTimeout;
const MAX_TIMER_DURATION_MS = 60 * 1000;
if (Platform.OS === 'android') {
  // Work around issue `Setting a timer for long time`
  // see: https://github.com/firebase/firebase-js-sdk/issues/97
  const timerFix = {};
  const runTask = (id, fn, ttl, args) => {
    const waitingTime = ttl - Date.now();
    if (waitingTime <= 1) {
      InteractionManager.runAfterInteractions(() => {
        if (!timerFix[id]) {
          return;
        }
        delete timerFix[id];
        fn(...args);
      });
      return;
    }

    const afterTime = Math.min(waitingTime, MAX_TIMER_DURATION_MS);
    timerFix[id] = _setTimeout(() => runTask(id, fn, ttl, args), afterTime);
  };

  global.setTimeout = (fn, time, ...args) => {
    if (MAX_TIMER_DURATION_MS < time) {
      const ttl = Date.now() + time;
      const id = '_lt_' + Object.keys(timerFix).length;
      runTask(id, fn, ttl, args);
      return id;
    }
    return _setTimeout(fn, time, ...args);
  };

  global.clearTimeout = id => {
    if (typeof id === 'string' && id.startWith('_lt_')) {
      _clearTimeout(timerFix[id]);
      delete timerFix[id];
      return;
    }
    _clearTimeout(id);
  };
}

class App extends Component {
  handleBackButton() {
    if (
      Actions.currentScene === 'home' ||
      Actions.currentScene === 'introslider'
    ) {
      BackHandler.exitApp();
    }
  }

  componentDidMount = async () => {
    StatusBar.setBarStyle('light-content', true);
    // codePush.sync({
    //   updateDialog: true,
    //   installMode: codePush.InstallMode.IMMEDIATE,
    // });
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
    AppState.addEventListener('change', async state => {
      console.log('AppState changed to', state);
      if (state === 'background') {
        if ((await getStorage('allow_notifications')) !== false) {
          await cancelInActiveNotification(); // cancel all previous inActive notifications
          inActiveUserNotification(); // set a new one ONLY
        }
      }
    });
  };

  render() {
    return (
      <MenuProvider skipInstanceCheck>
        <StatusBar
          barStyle="light-content"
          hidden={false}
          backgroundColor={'#707070'}
        />
        <Routes />
      </MenuProvider>
    );
  }
}
// let codePushOptions = {
//   checkFrequency: codePush.CheckFrequency.ON_APP_START,
//   installMode: codePush.InstallMode.IMMEDIATE,
// };

// export default connect((App = codePush(codePushOptions)(App)));
export default connect(App);
