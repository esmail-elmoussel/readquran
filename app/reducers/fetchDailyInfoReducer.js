import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  ayah: {},
  loading: false,
  errorMessage: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    // focus action is dispatched when a new screen comes into focus
    case actionTypes.FETCH_DAILY_AYA_TRIGGER:
      return {...state, loading: action.loading};
    case actionTypes.FETCH_DAILY_AYA_SUCCESS:
      return {
        ...state,
        aya: action.payload,
        loading: action.loading,
      };
    case actionTypes.FETCH_DAILY_AYA_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default: {
      return {
        ...state,
      };
    }
  }
}
