/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Header, Left, Button, Title, Body} from 'native-base';
import {StyleSheet, I18nManager} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/AntDesign';

class MinHeader extends Component {
  goBack = async () => {
    Actions.pop();
  };

  render() {
    const {title, showTitle, mainTitle} = this.props;
    return (
      <Header
        style={[
          styles.header,
          {borderBottomColor: !mainTitle ? '#DBDBDB' : 'transparent'},
        ]}>
        <Left style={styles.left}>
          <Button transparent onPress={() => this.goBack()}>
            <Icon
              style={styles.backButton}
              name={I18nManager.isRTL ? 'arrowright' : 'arrowleft'}
              size={25}
              color="#51565F"
            />
          </Button>
        </Left>
        <Body style={{display: !showTitle ? 'none' : 'flex'}}>
          <Title style={styles.title} allowFontScaling={false}>
            {title}
          </Title>
        </Body>
      </Header>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    borderBottomWidth: 1,
    shadowColor: 0,
    elevation: 0,
    alignItems: 'center',
    alignContent: 'center',
    lineHeight: 35,
    marginTop: 10,
    marginBottom: -20,
  },
  title: {
    color: '#12331F',
    alignSelf: 'center',
    lineHeight: 44,
    fontFamily: 'ElMessiriBold',
    fontSize: 25,
  },
  left: {
    width: 24,
    height: 24,
    position: 'absolute',
    left: 10,
    top: 3,
  },
  backButton: {
    width: 24,
    height: 24,
  },
});

export default MinHeader;
