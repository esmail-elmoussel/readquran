import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  hokm: {},
  loading: false,
  errorMessage: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_HOKM_TRIGGER:
      return {...state, loading: action.loading};
    case actionTypes.FETCH_HOKM_SUCCESS:
      return {
        ...state,
        hokm: action.payload,
        loading: action.loading,
      };
    case actionTypes.FETCH_HOKM_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default: {
      return {
        ...state,
      };
    }
  }
}
