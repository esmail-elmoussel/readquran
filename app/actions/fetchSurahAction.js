import {getAssetByKey} from '../axios/getService';
import * as actionTypes from './actionTypes';

// action body (higher order function)
const fetchSurah = Key => {
  // console.log(Key);
  // returning another function that takes dispatch and getState as arguments
  // dispatch used to dispatch redux actions
  // getState used to access store state -if needed- inside the function
  return async (dispatch, getState) => {
    //   validation if developer forgot to provide surah id
    if (!Key) {
      // returning promise reject to listen on while using the action body inside a component
      return Promise.reject(
        new Error("Please, make sure you've provided a surah id"),
      );
    }
    // dispatching the trigger action (could be used to show a loading icon)
    dispatch({type: actionTypes.FETCH_SURAH_TRIGGER, loading: true});
    // calling axios getService
    // const data = await getAssetByKey('sora/ayah', Key);
    return await getAssetByKey('sora/ayah', Key)
      .then(response => {
        //   if succeeded to fetch the data then we will need to dispatch the success action
        // success action sends the 'payload' to the associated reducer
        dispatch({
          type: actionTypes.FETCH_SURAH_SUCCESS,
          payload: response,
          loading: false,
        });
      })
      .catch(error => {
        //   dispatching fail action with error message
        dispatch({
          type: actionTypes.FETCH_SURAH_FAIL,
          payload: error.message,
          loading: false,
        });
      });
  };
};

export default fetchSurah;
