/* eslint-disable no-alert */
import axios from 'axios';
import {Actions} from 'react-native-router-flux';

export const sendEmail = async email => {
  const key = {
    email: email,
  };
  let data = '';
  await axios
    .post(
      'https://7astc125f5.execute-api.us-east-1.amazonaws.com/dev/user/forget-password/send-mail',
      key,
    )
    .then(res => {
      data = res.data;
    })
    .catch(err => {
      if (err.message === 'Network Error') {
        Actions.noInternet();
      } else if (err.message === 'Request failed with status code 400') {
        alert('invalid mail');
      }
    });
  return data;
};

export const updatePassword = async data => {
  let updated = '';
  await axios
    .post(
      'https://7astc125f5.execute-api.us-east-1.amazonaws.com/dev/user/forget-password/update-password',
      data,
    )
    .then(res => {
      if (res.data.includes('errorMessag')) {
        updated = false;
      } else {
        updated = true;
      }
    })
    // eslint-disable-next-line handle-callback-err
    .catch(err => {
      updated = false;
    });
  return updated;
};
