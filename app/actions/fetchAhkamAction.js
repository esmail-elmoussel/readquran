import {getAssetByKey} from '../axios/getService';
import postService from '../axios/postService';
import * as actionTypes from './actionTypes';

const fetchHokm = Key => {
  return async (dispatch, getState) => {
    if (!Key) {
      return Promise.reject(
        new Error("Please, make sure you've provided a surah id"),
      );
    }
    dispatch({type: actionTypes.FETCH_HOKM_TRIGGER, loading: true});
    // return await getAssetByKey('ahkam', Key)
    return await postService('user/get-all-ayat-related-to-hokm', Key)
      .then(response => {
        // console.log(JSON.stringify(response, null, 2));
        dispatch({
          type: actionTypes.FETCH_HOKM_SUCCESS,
          payload: response.data.body,
          loading: false,
        });
      })
      .catch(error => {
        dispatch({
          type: actionTypes.FETCH_HOKM_FAIL,
          payload: error.message,
          loading: false,
        });
      });
  };
};

export default fetchHokm;
