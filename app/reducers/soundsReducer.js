import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  soundsEnabled: true,
};

const soundsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_SOUNDS_STATE:
      return {...state, soundsEnabled: action.payload};
    default:
      return state;
  }
};

export default soundsReducer;
