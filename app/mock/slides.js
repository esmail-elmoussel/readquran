import {NativeModules} from 'react-native';

const locale = NativeModules.I18nManager.localeIdentifier.substring(0, 2);

const translate =
  locale === 'ar'
    ? require('../translations/ar.json')
    : locale === 'gr' || locale === 'de'
    ? require('../translations/gr.json')
    : locale === 'fr'
    ? require('../translations/fr.json')
    : locale === 'sp' || locale === 'es'
    ? require('../translations/sp.json')
    : require('../translations/en.json');

const slides = [
  {
    key: 'read',
    title: translate.Read_Quran,
    text: translate.You_can_choose_the_mood_to_read,
    image: require('../assets/img/slide1.png'),
  },
  {
    key: 'learn',
    title: translate.Learn_Tajweed,
    text: translate.Slide_Text1,
    image: require('../assets/img/slide2.png'),
  },
  {
    key: 'get_Encourage',
    title: translate.Get_Encouraged,
    text: translate.Slide_Text2,
    image: require('../assets/img/slide3.png'),
  },
];

export default slides;
