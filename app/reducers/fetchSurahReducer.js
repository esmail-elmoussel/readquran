import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  ayat: [],
  title: '',
  loading: false,
  errorMessage: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    // focus action is dispatched when a new screen comes into focus
    case actionTypes.FETCH_SURAH_TRIGGER:
      return {...state, loading: action.loading, ayat: []};
    case actionTypes.FETCH_SURAH_SUCCESS:
      return {
        ...state,
        ayat: action.payload,
        title: action.payload[0].sora_name_latin,
        loading: action.loading,
      };
    case actionTypes.FETCH_SURAH_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default: {
      return {
        ...state,
      };
    }
  }
}
