import AudioRecordModule from 'react-native-audio-recorder-player';
import RNFetchBlob from 'rn-fetch-blob';

const AudioRecord = new AudioRecordModule();

/**
 *
 *
 * @param {{fileLocation: string}} {fileLocation}
 * @returns {Promise<string>}
 */
export function getFileInBase64({fileLocation}) {
  return new Promise((resolve, reject) => {
    let data = '';
    RNFetchBlob.fs
      .readStream(
        // file path
        fileLocation,
        // encoding, should be one of `base64`, `utf8`, `ascii`
        'base64',
        4095,
      )
      .then(ifstream => {
        ifstream.open();
        ifstream.onData(chunk => {
          // when encoding is `ascii`, chunk will be an array contains numbers
          // otherwise it will be a string
          data += chunk;
        });

        ifstream.onError(reject);

        ifstream.onEnd(() => {
          resolve(data);
        });
      });
  });
}

/**
 *
 *
 * @export
 * @returns {{recordSecs, recordTime: string}}
 */
export function getAudio() {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await AudioRecord.startRecorder();
      AudioRecord.addRecordBackListener(e => {
        return resolve({
          recordSecs: e.current_position,
          recordTime: AudioRecord.mmssss(Math.floor(e.current_position)),
        });
      });
      console.log(`file will be in dir: ${result}`);
    } catch (error) {
      console.log(error);
    }
  });
}

export async function stopAudio() {
  try {
    const result = await AudioRecord.stopRecorder();
    AudioRecord.removeRecordBackListener();
    console.log(`Stopped file will be in dir: ${result}`);
    return {
      recordSecs: 0,
      base64: await getFileInBase64({fileLocation: result}),
    };
  } catch (error) {
    console.error(error);
  }
}
