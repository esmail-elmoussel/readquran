/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, Button} from 'native-base';
import {Circle} from 'react-native-animated-spinkit';
import {StyleSheet} from 'react-native';

class ImamButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {disabled, onPress, loading, buttonText} = this.props;
    return (
      <Button
        disabled={!disabled}
        style={[
          styles.button,
          {
            backgroundColor: !disabled ? '#C4C1C1' : '#478D73',
            borderColor: !disabled ? '#B5B5B5' : '#326351',
          },
        ]}
        onPress={async () => await onPress()}>
        <Circle
          size={25}
          color="white"
          style={{display: loading ? 'flex' : 'none'}}
        />
        <Text
          allowFontScaling={false}
          style={[
            styles.text,
            {
              color: !disabled ? '#989898' : 'white',
            },
          ]}>
          {buttonText}
        </Text>
      </Button>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    width: 300,
    height: 46,
    borderRadius: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    lineHeight: 22,
    minWidth: 88,
    borderWidth: 1,
    borderBottomWidth: 3,
    elevation: 0,
    marginTop: 25,
  },
  text: {
    color: '#FFFFFF',
    fontSize: 18,
    alignSelf: 'center',
    fontFamily: 'CairoRegular',
    lineHeight: 33,
    textTransform: 'capitalize',
  },
});

export default ImamButton;
