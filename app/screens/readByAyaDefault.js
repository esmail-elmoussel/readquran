/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Image, TouchableOpacity, I18nManager} from 'react-native';
import connect from '../connectedComponent/index';
import {Content, View, Text, Button} from 'native-base';
import FullAyaWordComponent from '../components/ayaView/fullAyaWord';
import AudioIcon from 'react-native-vector-icons/AntDesign';
import {Actions} from 'react-native-router-flux';
import Header from '../components/shared/MinHeader';
import Title from '../components/quranView/titleComponent';
import Mic from '../components/shared/micButton';
import AudioButton from '../components/ayaView/audioButton';
import {getAyaCorrection, check} from '../services/ayaService';
import PopupMessage from '../components/shared/message';
import {updateItem} from '../database/allSchemas';
import Loading from '../components/shared/Loading';
import {FlatList} from 'react-native-gesture-handler';
import Layout from '../components/shared/Layout';
import Spinner from 'react-native-loading-spinner-overlay';
import RNFS from 'react-native-fs';
import Share from 'react-native-share';

class ReadByAyaDefault extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: 'white',
      Tafsir: '',
      index: this.props?.selectedAya?.ayah_number,
      selectedAya: this.props?.selectedAya,
      results: '',
      correction: [],
      show: false,
      isRight: false,
      refreshLoading: false,
      showWordHokm: false,
      arabicWord: '',
      arabicWordHokm: [],
      message: '',
      shareDisabled: true,
      user_record_base64: '',
    };
  }

  getSpeechResult = async data => {
    this.setState({results: data.userText, user_record_base64: data.base64});
    const {selectedAya, results} = this.state;

    if (!data.userText) {
      alert('No data Found,Try Again');
    } else {
      try {
        const correction = await getAyaCorrection({
          sora_id: selectedAya.sora_number,
          ayah_number: selectedAya.ayah_number,
          user_text: results,
          audioBase64: data.base64,
        });
        // console.log(data.base64);
        const checkValue = await check(correction.data);
        this.setState({correction: correction.data});
        if (checkValue.length === 0) {
          this.setState({isRight: true, showWordHokm: false});
          this.bookmarkAya();
        } else {
          this.setState({isRight: false});
        }
        this.setState({show: true, shareDisabled: false});
      } catch (error) {
        Actions.noInternet({error: true});
      }
    }
  };

  bookmarkAya = () => {
    const item = {
      id: this.props.selectedAya.id,
      ayah_number: this.props.selectedAya.ayah_number,
    };
    updateItem(item)
      .then()
      .catch(error => {
        alert(`Something went wrong ${error}`);
      });
  };

  hideMessage = () => {
    this.setState({show: false, isRight: false});
    Actions.refresh();
  };

  ToggleNext = async () => {
    const {index} = this.state;
    const {selectedAya, sora} = this.props;

    if (index === selectedAya.number_of_ayat) {
      Actions.sora({selectedItem: sora});
      return;
    }
    await this.setState({refreshLoading: true, correction: [], show: false});
    await this.setState(prevState => ({
      index: prevState.index + 1,
    }));
    this.refreshData();
  };

  TogglePrev = async () => {
    const {index} = this.state;
    const {sora} = this.props;

    if (index === 1) {
      Actions.sora({selectedItem: sora});
      return;
    }
    await this.setState({refreshLoading: true, correction: [], show: false});
    await this.setState(prevState => ({
      index: prevState.index - 1,
    }));

    this.refreshData();
  };

  refreshData = async () => {
    const {index, selectedAya} = this.state;
    const {ayat, fetchAyah} = this.props;

    await this.setState({
      selectedAya: ayat[index - 1],
    });
    const requestData = {
      sora_id: selectedAya.sora_number,
      ayah_id: index,
    };
    await fetchAyah(requestData);
    Actions.refresh(this.setState({refreshLoading: false}));
  };

  toggleWordHokm = index => {
    if (this.state.correction[index]) {
      this.setState(state => {
        return {
          showWordHokm: true,
          arabicWord: state.correction[index].arabic_word,
          arabicWordHokm: state.correction[index].ahkam,
        };
      });
    }
  };

  share = () => {
    const {user_record_base64} = this.state;
    const path = `${RNFS.DocumentDirectoryPath}/audio.mp3`;
    RNFS.writeFile(path, user_record_base64, 'base64')
      .then(() => {
        Share.open({
          url: 'file://' + path,
          type: 'audio/mp3',
        });
      })
      .catch(console.log);
  };

  render() {
    const translate = this.props.language.text;
    const {fetchAyahWordsLoading, selectedAyaWords} = this.props;
    const {
      selectedAya,
      index,
      refreshLoading,
      correction,
      show,
      isRight,
      showWordHokm,
      arabicWord,
      arabicWordHokm,
      message,
      shareDisabled,
    } = this.state;

    return fetchAyahWordsLoading && selectedAyaWords.length === 0 ? (
      <Loading />
    ) : (
      <Layout>
        <Header title={''} mainTitle={true} />
        <Title
          allowFontScaling={false}
          selectedItem={selectedAya}
          showProgress={false}
        />
        <Content>
          <View style={styles.View}>
            <TouchableOpacity
              transparent
              style={styles.viewTitle}
              onPress={() => this.TogglePrev()}>
              <AudioIcon
                name={I18nManager.isRTL ? 'right' : 'left'}
                size={20}
                color={'#707070'}
                style={styles.left}
              />
            </TouchableOpacity>
            <View style={styles.viewTitle}>
              <Image
                source={require('../assets/img/soraIcon.png')}
                style={styles.basmalaImage}
              />
              <Text allowFontScaling={false} style={styles.ayaText}>
                {index}
              </Text>
              <Text
                allowFontScaling={false}
                style={[styles.ayaText, {fontFamily: 'CairoRegular', top: 25}]}>
                {index} /{selectedAya.number_of_ayat}
              </Text>
            </View>
            <TouchableOpacity
              transparent
              style={styles.viewTitle}
              onPress={() => this.ToggleNext()}>
              <AudioIcon
                name={I18nManager.isRTL ? 'left' : 'right'}
                size={20}
                color={'#707070'}
                style={styles.right}
              />
            </TouchableOpacity>
          </View>

          <Text allowFontScaling={false} style={styles.Text}>
            {translate.Start_reading_from_right_to_left}
          </Text>
          <View style={styles.ayaBody}>
            <FullAyaWordComponent
              correction={correction}
              toggleWordHokm={this.toggleWordHokm}
            />
            <Text allowFontScaling={false} style={styles.englishText}>
              {selectedAya.tafser}
            </Text>
          </View>

          {showWordHokm && arabicWord ? (
            <View>
              <Text style={styles.wrongWord}>{arabicWord}</Text>
              {arabicWordHokm ? (
                <FlatList
                  horizontal
                  data={arabicWordHokm}
                  renderItem={({item}) => {
                    return <Text style={styles.wrongWordHokm}>{item}</Text>;
                  }}
                  contentContainerStyle={styles.wrongWordAhkam}
                  keyExtractor={(item, indexx) => indexx.toString()}
                />
              ) : null}
            </View>
          ) : null}

          <View style={[styles.buttonView, {display: show ? 'none' : 'flex'}]}>
            <Mic
              getSpeechResult={this.getSpeechResult}
              micPressed={() => this.setState({shareDisabled: true})}
            />
            <AudioButton
              sora={selectedAya.sora_number}
              ayah={selectedAya.ayah_number}
            />
          </View>
          <PopupMessage
            isRight={isRight}
            show={show}
            onNextPress={this.ToggleNext}
            onTryAgainPress={this.hideMessage}
          />
        </Content>
        <Button
          disabled={shareDisabled}
          style={[
            styles.shareButton,
            shareDisabled && styles.shareButtonDisabled,
          ]}
          onPress={this.share}>
          <Image
            style={styles.shareIcon}
            source={require('../assets/img/icon/share.png')}
          />
        </Button>
        <Spinner visible={refreshLoading} />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  Text: {
    fontSize: 14,
    fontFamily: 'CairoRegular',
    color: '#707070',
    paddingHorizontal: 10,
    lineHeight: 26,
  },
  basmalaImage: {
    width: 26,
    height: 26,
    alignSelf: 'center',
  },
  left: {
    alignSelf: 'flex-start',
    marginLeft: 15,
  },
  right: {
    alignSelf: 'flex-end',
    marginRight: 15,
  },
  ayaText: {
    position: 'absolute',
    alignSelf: 'center',
    fontSize: 7,
    color: '#707070',
    top: 7,
    lineHeight: 13,
    fontFamily: 'Cairo-Bold',
  },
  viewTitle: {
    width: '33%',
  },
  View: {
    flexDirection: 'row',
    backgroundColor: '#F8F8F8',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 5,
    marginTop: 10,
    marginBottom: 10,
  },
  ayaBody: {
    paddingTop: 10,
    paddingBottom: 25,
    marginTop: 10,
    backgroundColor: '#f7f6f1',
    borderColor: '#C9AD28',
    borderWidth: 1,
    width: '95%',
    alignSelf: 'center',
    borderRadius: 11,
    alignItems: 'center',
    padding: 5,
  },
  englishText: {
    fontFamily: 'CairoRegular',
    fontSize: 14,
    lineHeight: 26,
    color: '#707070',
    textAlign: 'center',
    paddingHorizontal: 10,
  },
  wrongWord: {
    fontFamily: 'Amiri',
    fontSize: 30,
    textAlign: 'center',
    color: '#D64944',
  },
  wrongWordHokm: {
    fontFamily: 'CairoRegular',
    fontSize: 14,
    textAlign: 'center',
    color: '#444444',
    marginHorizontal: 5,
  },
  buttonView: {
    height: 150,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  wrongWordAhkam: {
    width: '100%',
    justifyContent: 'center',
  },
  shareButton: {
    position: 'absolute',
    right: 20,
    bottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    width: 70,
    height: 70,
    alignSelf: 'center',
    borderRadius: 100,
  },
  shareButtonDisabled: {
    opacity: 0.6,
  },
  shareIcon: {
    width: 40,
    height: 40,
  },
});

export default connect(ReadByAyaDefault);
