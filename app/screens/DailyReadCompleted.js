import React, {Component} from 'react';
import {StyleSheet, Text, Image} from 'react-native';
import {View, Button} from 'native-base';
import Layout from '../components/shared/Layout';
import {Actions} from 'react-native-router-flux';

class DailyReadCompleted extends Component {
  render() {
    return (
      <Layout>
        <View style={styles.container}>
          <Image source={require('../assets/img/imamlaughing.png')} />
          <Text style={styles.text}>
            Congratulations, you've completed your daily read today.
          </Text>
          <Button onPress={() => Actions.quran()} style={styles.button}>
            <Text style={styles.text}>Continue Reading</Text>
          </Button>
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'Amiri',
    fontSize: 24,
    color: '#707070',
    textAlign: 'center',
    marginHorizontal: 10,
  },
  button: {
    backgroundColor: 'transparent',
    borderRadius: 20,
    marginTop: 30,
  },
});

export default DailyReadCompleted;
