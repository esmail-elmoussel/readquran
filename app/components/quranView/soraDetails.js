/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {StyleSheet, I18nManager} from 'react-native';
import {Text, View} from 'native-base';
import SoraIcon from './soraICon';

class SoraDetails extends Component {
  render() {
    const {sora} = this.props;

    return I18nManager.isRTL ? (
      <View style={styles.listBody}>
        <View style={styles.arabicView}>
          <Text
            allowFontScaling={false}
            style={[
              styles.soraNumber,
              {fontSize: 20, position: 'absolute', left: 0},
            ]}>
            {sora.sora_number}
          </Text>
          <SoraIcon sora={sora.place} />
          <Text
            allowFontScaling={false}
            style={[styles.soraNameArabic, {textAlign: 'right'}]}>
            {sora.sora_name_arabic.replace('سورة', '')}
          </Text>
        </View>
        <View style={[styles.englishView, {width: '50%'}]}>
          <Text allowFontScaling={false} style={styles.soraNameEnglish}>
            {sora.sora_name_latin}
          </Text>
          <Text allowFontScaling={false} style={styles.englishTranslate}>
            {sora.sora_name_english}
          </Text>
        </View>
      </View>
    ) : (
      <View style={styles.listBody}>
        <View style={styles.englishView}>
          <Text allowFontScaling={false} style={styles.soraNameEnglish}>
            {sora.sora_name_latin}
          </Text>
          <Text allowFontScaling={false} style={styles.englishTranslate}>
            {sora.sora_name_english}
          </Text>
        </View>
        <View style={styles.arabicView}>
          <Text allowFontScaling={false} style={styles.soraNameArabic}>
            {sora.sora_name_arabic.replace('سورة', '')}
          </Text>
          <Text
            allowFontScaling={false}
            style={[
              styles.soraNumber,
              {fontSize: 20, marginLeft: 10, marginBottom: 3},
            ]}>
            {sora.sora_number}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listBody: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  englishView: {
    flexDirection: 'column',
    width: '45%',
  },
  soraNameEnglish: {
    fontFamily: 'ElMessiriBold',
    fontSize: 17,
    lineHeight: 21,
    color: '#707070',
  },
  englishTranslate: {
    fontFamily: 'segoe-ui',
    fontSize: 11,
    lineHeight: 15,
    color: '#707070',
  },
  arabicView: {
    flexDirection: 'row',
    width: '45%',
    justifyContent: 'flex-end',
  },
  soraNameArabic: {
    fontSize: 25,
    color: '#707070',
    // alignSelf: 'center',
    fontFamily: 'ElMessiri-Regular',
    // textAlign: 'right',
  },
  soraNumber: {
    fontSize: 25,
    color: '#707070',
    alignSelf: 'center',
    fontWeight: 'bold',
  },
});

export default SoraDetails;
