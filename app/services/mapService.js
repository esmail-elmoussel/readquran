import postService from '../axios/postService';
import {getStorage} from './localStorageManager';

const updateMapProgress = async ({
  sora_id,
  sora_reason_step,
  small_sora_step,
  telawa_step,
  tagweed_step,
  recite,
  score,
}) => {
  const user_id = (await getStorage('data')).user.id;
  const response = await postService('user/update-map-progress', {
    user_id,
    sora_id,
    sora_reason_step,
    small_sora_step,
    telawa_step,
    tagweed_step,
    recite,
    score,
  });
  if (response.status !== 200) {
    console.log('error');
  }
  console.log('res', response.data);
  return response.data[0];
};

export default updateMapProgress;
