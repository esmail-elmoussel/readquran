import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  questions: [],
  loading: false,
  errorMessage: null,
};

const fetchAyaReasonReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_RECIT_QUIZ_TRIGGER:
      return {...state, loading: action.loading, questions: []};
    case actionTypes.GET_RECIT_QUIZ_SUCESS:
      return {
        ...state,
        questions: action.payload,
        loading: action.loading,
      };
    case actionTypes.GET_RECIT_QUIZ_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default:
      return state;
  }
};

export default fetchAyaReasonReducer;
