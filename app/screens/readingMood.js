/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Content, View, Text} from 'native-base';
import connect from '../connectedComponent/index';
import Header from '../components/shared/MinHeader';
import Title from '../components/quranView/titleComponent';
import {Actions} from 'react-native-router-flux';
import {setStorage} from '../services/localStorageManager';
import Layout from '../components/shared/Layout';

class ReadingMood extends React.Component {
  UNSAFE_componentWillMount = () => {
    const Key = {
      soraId: this.props.selectedItem.sora_number,
    };
    this.props.fetchSurah(Key);
  };

  getAllAyat = mode => {
    requestAnimationFrame(() => {
      const data = mode;
      setStorage({key: 'readingMode', data});
      Actions.sora({selectedItem: this.props.selectedItem, mode: mode});
    });
  };

  render() {
    const translate = this.props?.language?.text;

    return (
      <Layout>
        <Header title={translate.Reading_Mood} showTitle={true} />
        <Content style={styles.content}>
          <Title
            allowFontScaling={false}
            selectedItem={this.props.selectedItem}
            showProgress={true}
          />
          <View style={styles.bodyView}>
            <Text allowFontScaling={false} style={styles.Text}>
              {translate.Choose_the_way_you_read}
            </Text>
          </View>
          <View>
            <TouchableOpacity
              style={styles.TouchableOpacity}
              onPress={() => this.getAllAyat('word')}>
              <Text allowFontScaling={false} style={styles.buttonText}>
                {translate.Word_by_word}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.TouchableOpacity}
              onPress={() => this.getAllAyat('aya')}>
              <Text allowFontScaling={false} style={styles.buttonText}>
                {translate.Aya_by_Aya}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.TouchableOpacity,
                {backgroundColor: '#E5E4E4', borderColor: '#E5E4E4'},
              ]}
              disabled={true}>
              <Text
                allowFontScaling={false}
                style={[styles.buttonText, {color: '#707070'}]}>
                {translate.Meaning_and_quiz}
              </Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    marginTop: 10,
  },
  Text: {
    fontSize: 22,
    fontFamily: 'CairoRegular',
    lineHeight: 42,
    color: '#707070',
    alignSelf: 'center',
    marginBottom: 15,
  },
  bodyView: {
    marginTop: 15,
  },
  TouchableOpacity: {
    height: 51,
    width: '90%',
    borderWidth: 1,
    borderColor: '#326351',
    marginBottom: 10,
    borderRadius: 11,
    justifyContent: 'center',
    backgroundColor: '#478D73',
    alignSelf: 'center',
    borderBottomWidth: 3,
  },
  buttonText: {
    alignSelf: 'center',
    fontSize: 21,
    textTransform: 'capitalize',
    fontFamily: 'CairoRegular',
    lineHeight: 42,
    color: 'white',
  },
});

export default connect(ReadingMood);
