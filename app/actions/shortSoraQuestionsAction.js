import postService from '../axios/postService';
import * as actionTypes from './actionTypes';

const getQuestions = async data => {
  // console.log('gjj');
  const promises = [];
  let i = 1;
  for (i; i <= data.numberOfAyahs; i++) {
    const key = {
      sora_id: data.sora_id,
      ayah_number: i,
    };
    // console.log(key);
    await postService('user/small-surah', key)
      .then(res => {
        // console.log(res.data.length);
        res.data.map(item => {
          promises.push(item);
        });
      })
      .catch(err => {
        console.log(JSON.stringify(err, null, 2));
      });
  }
  // console.log(JSON.stringify(promises, null, 2));
  const questions = await Promise.all(promises);
  const shuffleQuestion = await shuffleArray(questions);
  return shuffleQuestion;
};

const shuffleArray = array => {
  let i = array.length - 1;
  for (; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
};

const fetchShortSoraQuesion = data => {
  // console.log(data);
  return async (dispatch, getState) => {
    if (!data) {
      return Promise.reject(
        new Error("Please, make sure you've provided request data"),
      );
    }
    dispatch({
      type: actionTypes.FETCH_SHORT_SURAH_QUESTION_TRIGGER,
      loading: true,
    });
    const questions = await getQuestions(data);
    if (questions.length > 0) {
      dispatch({
        type: actionTypes.FETCH_SHORT_SURAH_QUESTION_SUCCESS,
        payload: questions,
        loading: false,
      });
    } else {
      dispatch({
        type: actionTypes.FETCH_SHORT_SURAH_QUESTION_FAIL,
        payload: 'something wnt wrong',
        loading: false,
      });
    }
  };
};

export default fetchShortSoraQuesion;
