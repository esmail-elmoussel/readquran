import postService from '../axios/postService';
import * as actionTypes from './actionTypes';
import {getStorage} from '../services/localStorageManager';
import {insertNewItem, deleteAllItems} from '../database/allSchemas';

const fetchSuar = () => {
  return async (dispatch, getState) => {
    const data = await getStorage('data');
    const requestData = {
      user_id: data.user.id,
    };
    dispatch({type: actionTypes.FETCH_SUAR_TRIGGER, loading: true});
    return await postService('user/get-progress', requestData)
      .then(response => {
        deleteAllItems();
        response.data.map(item => {
          insertNewItem(item);
        });
        dispatch({
          type: actionTypes.FETCH_SUAR_SUCCESS,
          payload: response.data,
          loading: false,
        });
      })
      .catch(error => {
        dispatch({
          type: actionTypes.FETCH_SUAR_FAIL,
          payload: error.message,
          loading: false,
        });
      });
  };
};

export default fetchSuar;
