import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  words: [],
  loading: false,
  errorMessage: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    // focus action is dispatched when a new screen comes into focus
    case actionTypes.FETCH_AYAH_TRIGGER:
      return {...state, loading: action.loading};
    case actionTypes.FETCH_AYAH_SUCCESS:
      return {
        ...state,
        words: action.payload,
        loading: action.loading,
      };
    case actionTypes.RECORD_NOT_FOUND:
      return {
        ...state,
        loading: action.loading,
      };
    case actionTypes.FETCH_AYAH_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default: {
      return {
        ...state,
      };
    }
  }
}
