/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Image, TouchableOpacity, I18nManager} from 'react-native';
import connect from '../connectedComponent/index';
import {Content, View, Text} from 'native-base';
import Footer from '../components/ayaView/fullAyaFooter';
import WordComponent from '../components/ayaView/word';
import AudioIcon from 'react-native-vector-icons/AntDesign';
import {Actions} from 'react-native-router-flux';
import Header from '../components/shared/MinHeader';
import Title from '../components/quranView/titleComponent';
import Mic from '../components/shared/micButton';
import AudioButton from '../components/ayaView/audioButton';
import {getWordCorrection} from '../services/ayaService';
import PopupMessage from '../components/shared/message';
import postService from '../axios/postService';
import {getStorage, setStorage} from '../services/localStorageManager';
import Aya from '../components/ayaView/ayaComponent';
import Layout from '../components/shared/Layout';
import Spinner from 'react-native-loading-spinner-overlay';

class ReadByWord extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: 'white',
      Tafsir: '',
      wordIndex: 0,
      selectedAya: this.props.selectedAya,
      result: '',
      correction: '',
      show: false,
      isRight: false,
      index: this.props.selectedAya.ayah_number,
      refreshLoading: false,
      ayaWords: this.props?.selectedAyaWords.slice(0, 7),
    };
  }

  getWordIndex = async index => {
    await this.setState({wordIndex: index});
    Actions.refresh(this.setState({correction: ''}));
  };

  getSpeechResult = async data => {
    const translate = this.props?.language?.text;

    if (!data.userText) {
      alert(translate.No_data_Found_Try_Again);
    }
    this.setState({result: data.userText});
    try {
      const correction = await getWordCorrection({
        sora_id: this.state.selectedAya.sora_number,
        ayah_number: this.state.selectedAya.ayah_number,
        user_text: this.state.result,
        word_index: this.state.wordIndex + 1,
        audioBase64: data.base64,
      });
      this.setState({correction: correction.data});
      if (this.state.correction === true) {
        this.setState({isRight: true});
        this.updateReadCounter();
      } else {
        this.setState({isRight: false});
      }
      this.setState({show: true});
      this.hideMessage();
    } catch (error) {
      Actions.noInternet({error: true});
    }
  };

  updateReadCounter = async () => {
    const Key = {
      user_id: {...(await getStorage('data'))}.user.id,
      add: this.state.result.length,
    };
    await postService('user/update-read-counter', Key).then(
      async res => {
        const data = {
          token: {...(await getStorage('data'))}.token,
          user: JSON.parse(res.data),
        };
        await setStorage({key: 'data', data});
        this.props.updateUserData();
      },
      err => {
        console.log(err);
      },
    );
  };

  hideMessage = async () => {
    setTimeout(() => {
      this.setState({show: false, isRight: false});
      Actions.refresh();
    }, 2000);
  };

  ToggleNext = async () => {
    if (this.state.index === this.props.selectedAya.number_of_ayat) {
      Actions.sora({selectedItem: this.props.sora});
      return;
    }
    await this.setState({refreshLoading: true, correction: ''});
    await this.setState(prevState => ({
      index: prevState.index + 1,
    }));

    this.refreshData();
  };

  TogglePrev = async () => {
    if (this.state.index === 1) {
      Actions.sora({selectedItem: this.props.sora});
      return;
    }
    await this.setState({refreshLoading: true, correction: ''});
    await this.setState(prevState => ({
      index: prevState.index - 1,
    }));

    this.refreshData();
  };

  refreshData = async () => {
    await this.setState({
      selectedAya: this.props.ayat[this.state.index - 1],
    });
    const requestData = {
      sora_id: this.state.selectedAya.sora_number,
      ayah_id: this.state.index,
    };
    await this.props.fetchAyah(requestData);
    Actions.refresh(this.setState({refreshLoading: false}));
  };

  render() {
    const translate = this.props.language.text;
    const {fetchAyahWordsLoading} = this.props;
    const {refreshLoading} = this.state;

    return (
      <Layout>
        <Header title={''} mainTitle={true} />
        <Title
          allowFontScaling={false}
          selectedItem={this.state.selectedAya}
          showProgress={false}
        />
        <Content>
          <View style={styles.View}>
            <TouchableOpacity
              transparent
              style={styles.viewTitle}
              onPress={() => this.TogglePrev()}>
              <AudioIcon
                name={I18nManager.isRTL ? 'right' : 'left'}
                size={20}
                color={'#707070'}
                style={styles.left}
              />
            </TouchableOpacity>
            <View style={styles.viewTitle}>
              <Image
                source={require('../assets/img/soraIcon.png')}
                style={styles.basmalaImage}
              />
              <Text allowFontScaling={false} style={styles.ayaText}>
                {this.state.index}
              </Text>
              <Text
                allowFontScaling={false}
                style={[styles.ayaText, {top: 25}]}>
                {this.state.index} /{this.state.selectedAya.number_of_ayat}
              </Text>
            </View>
            <TouchableOpacity
              transparent
              style={styles.viewTitle}
              onPress={() => this.ToggleNext()}>
              <AudioIcon
                name={I18nManager.isRTL ? 'left' : 'right'}
                size={20}
                color={'#707070'}
                style={styles.right}
              />
            </TouchableOpacity>
          </View>
          <Text allowFontScaling={false} style={styles.Text}>
            {translate.Start_reading_from_right_to_left}
          </Text>
          <View style={styles.aya}>
            <Aya index={this.state.wordIndex} />
          </View>
          <View style={styles.ayaBody}>
            <WordComponent
              getWordIndex={this.getWordIndex}
              correction={this.state.correction}
              index={this.state.wordIndex}
            />
          </View>
          <View
            style={[
              styles.buttonView,
              {display: this.state.show ? 'none' : 'flex'},
            ]}>
            <Mic getSpeechResult={this.getSpeechResult} />
            <AudioButton
              sora={this.state.selectedAya.sora_number}
              ayah={this.state.selectedAya.ayah_number}
              wordUrl={
                this.props?.selectedAyaWords[this.state.wordIndex]?.wordUrl
              }
            />
          </View>
          <PopupMessage
            isRight={this.state.isRight}
            show={this.state.show}
            ayaMessage={true}
          />
        </Content>
        <Spinner visible={refreshLoading || fetchAyahWordsLoading} />
        <Footer />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  Text: {
    fontSize: 14,
    fontFamily: 'CairoRegular',
    color: '#707070',
    paddingHorizontal: 10,
    lineHeight: 26,
    marginVertical: 5,
  },
  basmalaImage: {
    width: 26,
    height: 26,
    alignSelf: 'center',
  },
  left: {
    alignSelf: 'flex-start',
    marginLeft: 15,
  },
  right: {
    alignSelf: 'flex-end',
    marginRight: 15,
  },
  ayaText: {
    position: 'absolute',
    alignSelf: 'center',
    fontSize: 7,
    color: '#707070',
    top: 7,
    lineHeight: 13,
    fontFamily: 'Cairo-Bold',
  },
  viewTitle: {
    width: '33%',
  },
  View: {
    flexDirection: 'row',
    backgroundColor: '#F8F8F8',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 5,
    marginTop: 10,
  },
  ayaBody: {
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 25,
    backgroundColor: '#f7f6f1',
    borderColor: '#C9AD28',
    borderWidth: 1,
    width: '95%',
    alignSelf: 'center',
    borderRadius: 11,
    alignItems: 'center',
    padding: 5,
  },
  englishText: {
    fontFamily: 'CairoRegular',
    fontSize: 12,
    lineHeight: 20,
    color: '#707070',
    textAlign: 'center',
    marginTop: 25,
    width: '95%',
    alignSelf: 'center',
  },
  buttonView: {
    height: 150,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  aya: {
    backgroundColor: '#F8F8F8',
    alignItems: 'center',
    paddingVertical: 10,
  },
});

export default connect(ReadByWord);
