import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  ImageBackground,
  I18nManager,
} from 'react-native';
import {Text, View} from 'native-base';
import connect from '../../connectedComponent/index';
import {Actions} from 'react-native-router-flux';
import {getStorage} from '../../services/localStorageManager';

class AyaList extends Component {
  constructor() {
    super();
    this.state = {
      readingMode: '',
    };
  }

  async UNSAFE_componentWillMount() {
    const readingMode = await getStorage('readingMode');
    await this.setState({readingMode: readingMode});
  }

  getAllWords = async aya => {
    requestAnimationFrame(() => {
      if (this.state.readingMode === 'aya') {
        Actions.readByAyaDefault({
          selectedAya: aya,
          sora: this.props.sora,
        });
      } else {
        Actions.readByWord({
          selectedAya: aya,
          sora: this.props.sora,
        });
      }
      const requestData = {
        sora_id: aya.sora_number,
        ayah_id: aya.ayah_number,
      };
      this.props.fetchAyah(requestData);
    });
  };

  render() {
    return (
      <TouchableHighlight style={styles.container}>
        <TouchableOpacity
          style={styles.TouchableOpacity}
          onPress={() => {
            this.getAllWords(this.props.aya);
          }}>
          <View style={styles.arabicView}>
            <ImageBackground
              source={require('../../assets/img/soraIcon.png')}
              style={styles.ayah_number}>
              <Text allowFontScaling={false} style={styles.ayaText}>
                {this.props.aya.ayah_number}
              </Text>
            </ImageBackground>
            <Text allowFontScaling={false} style={styles.itemListText}>
              {this.props.aya.arabic_recitation}
            </Text>
          </View>
          <View>
            <View style={styles.englishView}>
              <ImageBackground
                source={require('../../assets/img/soraIcon.png')}
                style={styles.ayah_number}>
                <Text allowFontScaling={false} style={styles.ayaText}>
                  {this.props.aya.ayah_number}
                </Text>
              </ImageBackground>
              <Text allowFontScaling={false} style={styles.frankoText}>
                {this.props.aya.franko_recitation}
              </Text>
            </View>
            <Text allowFontScaling={false} style={styles.translateText}>
              {this.props.language === 'tr'
                ? this.props.aya.turkish_slang
                : this.props.language === 'ru'
                ? this.props.aya.russian_slang
                : this.props.aya.english_recitation}
            </Text>
          </View>
        </TouchableOpacity>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  itemListText: {
    fontSize: 20,
    color: '#707070',
    lineHeight: 35,
    fontFamily: 'Amiri',
    marginRight: 25,
    padding: 15,
    marginLeft: I18nManager.isRTL ? 15 : 0,
  },
  frankoText: {
    fontSize: 17,
    fontFamily: 'CairoRegular',
    color: '#707070',
    padding: 5,
    lineHeight: 32,
    marginLeft: 35,
    marginRight: I18nManager.isRTL ? 35 : 0,
  },
  translateText: {
    fontSize: 10,
    fontFamily: 'CairoRegular',
    color: '#707070',
    lineHeight: 15,
    marginLeft: 40,
    marginRight: I18nManager.isRTL ? 40 : 0,
  },
  container: {
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#DBDBDB',
    width: '95%',
    alignSelf: 'center',
  },
  ayah_number: {
    width: 26,
    height: 26,
    alignSelf: 'center',
    position: 'absolute',
  },
  arabicView: {
    flexDirection: I18nManager.isRTL ? 'row' : 'row-reverse',
    alignSelf: I18nManager.isRTL ? 'flex-start' : 'flex-end',
  },
  ayaText: {
    position: 'absolute',
    alignSelf: 'center',
    fontSize: 7,
    color: '#707070',
    top: 5,
    lineHeight: 15,
    fontFamily: 'CairoBold',
  },
  englishView: {flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row'},
  TouchableOpacity: {width: '100%'},
});

export default connect(AyaList);
