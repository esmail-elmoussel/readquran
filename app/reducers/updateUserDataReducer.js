import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  user: {},
};

const intlLanguage = (state = initialState, action) => {
  if (action === undefined) {
    return state;
  }
  switch (action.type) {
    case actionTypes.FETCH_USER:
      return {
        user: action.user,
      };
    default:
      return state;
  }
};
export default intlLanguage;
