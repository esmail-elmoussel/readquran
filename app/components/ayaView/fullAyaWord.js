/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text} from 'native-base';
import {StyleSheet, View, I18nManager, FlatList} from 'react-native';
import ReversedFlatList from 'react-native-reversed-flat-list';
import connect from '../../connectedComponent/index';

class FullAyaWordComponent extends Component {
  constructor() {
    super();
    this.state = {
      index: 0,
      active: '',
      showModal: false,
    };
  }

  formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);
    let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
    while (
      numberOfElementsLastRow !== numColumns &&
      numberOfElementsLastRow !== 0
    ) {
      data.push({key: `blank-${numberOfElementsLastRow}`, empty: true});
      numberOfElementsLastRow++;
    }
    return data;
  };

  renderItem = ({item}) => {
    return (
      <View style={styles.ListItem}>
        <Text
          allowFontScaling={false}
          style={[
            styles.arabicText,
            {
              fontSize: this.props.map ? 40 : 30,
              lineHeight: this.props.map ? 90 : 53,
              fontFamily: this.props.map ? 'aldhabi' : 'Amiri',
              color:
                this.props.correction.length === 0
                  ? this.props.map
                    ? '#5D2302'
                    : '#707070'
                  : this.props.correction.length > 0 && item.status === 'true'
                  ? this.props.map
                    ? '#323F1E'
                    : '#59AC4F'
                  : this.props.map
                  ? '#A00306'
                  : '#D64944',
            },
          ]}>
          {item.arabic_word}
        </Text>
        <Text
          allowFontScaling={false}
          style={[
            styles.Text,
            {
              fontSize: this.props.map ? 26 : 16,
              fontFamily: this.props.map ? 'aldhabi' : 'CairoRegular',
              color:
                this.props.correction.length === 0
                  ? this.props.map
                    ? '#5D2302'
                    : '#707070'
                  : this.props.correction.length > 0 && item.status === 'true'
                  ? this.props.map
                    ? '#323F1E'
                    : '#59AC4F'
                  : this.props.map
                  ? '#A00306'
                  : '#D64944',
            },
          ]}>
          {item.english_slang}
        </Text>
        <Text
          allowFontScaling={false}
          style={[
            styles.Text,
            {
              fontSize: this.props.map ? 15 : 9,
              fontFamily: this.props.map ? 'aldhabi' : 'CairoRegular',
              color:
                this.props.correction.length === 0
                  ? this.props.map
                    ? '#5D2302'
                    : '#707070'
                  : this.props.correction.length > 0 && item.status === 'true'
                  ? this.props.map
                    ? '#323F1E'
                    : '#59AC4F'
                  : this.props.map
                  ? '#A00306'
                  : '#D64944',
            },
          ]}>
          {this.props.language === 'turkish'
            ? item.turkish_slang
            : this.props.language === 'russian'
            ? item.russian_slang
            : item.english_word}
        </Text>
      </View>
    );
  };

  render() {
    const numColumns = 4;
    if (this.props.correction.length > 0) {
      return I18nManager.isRTL ? (
        <FlatList
          data={this.formatData(this.props.correction, numColumns)}
          contentContainerStyle={styles.list}
          renderItem={this.renderItem}
          numColumns={numColumns}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <ReversedFlatList
          data={this.formatData(this.props.correction, numColumns)}
          contentContainerStyle={styles.list}
          renderItem={this.renderItem}
          numColumns={numColumns}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    } else {
      return I18nManager.isRTL ? (
        <FlatList
          data={this.formatData(this.props.selectedAyaWords, numColumns)}
          contentContainerStyle={styles.list}
          renderItem={this.renderItem}
          numColumns={numColumns}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <ReversedFlatList
          data={this.formatData(this.props.selectedAyaWords, numColumns)}
          contentContainerStyle={styles.list}
          renderItem={this.renderItem}
          numColumns={numColumns}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  list: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    marginVertical: 15,
  },
  ListItem: {
    flexDirection: 'column',
    marginHorizontal: 4,
  },
  arabicText: {
    fontSize: 30,
    color: '#707070',
    lineHeight: 53,
    fontFamily: 'Amiri',
    alignSelf: 'center',
  },
  Text: {
    fontSize: 16,
    fontFamily: 'CairoRegular',
    color: '#707070',
    alignSelf: 'center',
    lineHeight: 30,
  },
});

export default connect(FullAyaWordComponent);
