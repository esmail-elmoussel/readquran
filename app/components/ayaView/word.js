/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {ListItem, Text, View} from 'native-base';
import {StyleSheet, TouchableOpacity, I18nManager} from 'react-native';
import connect from '../../connectedComponent/index';
import AudioIcon from 'react-native-vector-icons/AntDesign';

class WordComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      showModal: false,
      index: this.props.index,
    };
  }

  ToggleNext = async () => {
    if (this.state.index === this.props.selectedAyaWords.length - 1) {
      return;
    }
    await this.setState(prevState => ({
      index: prevState.index + 1,
    }));
    this.props.getWordIndex(this.state.index);
  };

  TogglePrev = async () => {
    if (this.state.index === 0) {
      return;
    }

    await this.setState(prevState => ({
      index: prevState.index - 1,
    }));
    this.props.getWordIndex(this.state.index);
  };

  renderListItem = () => {
    const item = this.props?.selectedAyaWords[this.state.index];

    return (
      <ListItem style={styles.ListItem}>
        <Text
          allowFontScaling={false}
          style={[
            styles.arabicText,
            {
              color:
                this.props.correction === ''
                  ? '#707070'
                  : this.props.correction
                  ? '#59AC4F'
                  : '#D64944',
            },
          ]}>
          {item?.arabic_word}
        </Text>
        <Text
          allowFontScaling={false}
          style={[
            styles.Text,
            {
              color:
                this.props.correction === ''
                  ? '#707070'
                  : this.props.correction
                  ? '#59AC4F'
                  : '#D64944',
            },
          ]}>
          {item?.english_slang}
        </Text>
        <Text
          allowFontScaling={false}
          style={[
            styles.Text,
            {
              fontSize: 14,
              color:
                this.props.correction === ''
                  ? '#707070'
                  : this.props.correction
                  ? '#59AC4F'
                  : '#D64944',
            },
          ]}>
          {this.props.language === 'turkish'
            ? item?.turkish_slang
            : this.props.language === 'russian'
            ? item?.russian_slang
            : item?.english_word}
        </Text>
      </ListItem>
    );
  };

  render() {
    return (
      <View style={styles.View}>
        <TouchableOpacity
          transparent
          style={styles.icon}
          onPress={() => this.TogglePrev()}>
          <AudioIcon
            name={I18nManager.isRTL ? 'right' : 'left'}
            size={20}
            color={'#707070'}
            style={styles.left}
          />
        </TouchableOpacity>
        {this.renderListItem()}
        <TouchableOpacity
          transparent
          style={styles.icon}
          onPress={() => this.ToggleNext()}>
          <AudioIcon
            name={I18nManager.isRTL ? 'left' : 'right'}
            size={20}
            color={'#707070'}
            style={styles.right}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ListItem: {
    flexDirection: 'column',
    borderBottomWidth: 0,
    marginLeft: 0,
    alignSelf: 'center',
  },
  arabicText: {
    fontSize: 30,
    color: '#707070',
    lineHeight: 53,
    fontFamily: 'Amiri',
    alignSelf: 'center',
  },
  Text: {
    fontSize: 16,
    fontFamily: 'CairoRegular',
    color: '#707070',
    alignSelf: 'center',
    lineHeight: 30,
  },
  View: {
    flexDirection: 'row',
    backgroundColor: '#F8F8F8',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '95%',
  },
  icon: {width: 25, height: 25},
});

export default connect(WordComponent);
