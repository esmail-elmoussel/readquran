/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Image} from 'react-native';
import {View, Title, Text} from 'native-base';
import connect from '../../connectedComponent/index';

class TitleComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {selectedItem, showProgress} = this.props;
    return (
      <View>
        <View
          style={[styles.image, {display: !showProgress ? 'none' : 'flex'}]}>
          <Image
            source={require('../../assets/img/soraIcon.png')}
            style={styles.soraIcon}
          />
          {selectedItem.place === 'Makkah' ? (
            <Image
              source={require('../../assets/img/Makkah.png')}
              style={styles.soraType}
            />
          ) : (
            <Image
              source={require('../../assets/img/Madinah.png')}
              style={styles.soraType}
            />
          )}
        </View>
        <Title
          allowFontScaling={false}
          style={[styles.title, {marginTop: !showProgress ? -35 : 0}]}>
          {selectedItem?.sora_name_arabic?.replace('سورة', '')}
        </Title>
        <Text allowFontScaling={false} style={[styles.title, styles.text]}>
          {selectedItem.sora_name_latin}
        </Text>
        <Text allowFontScaling={false} style={styles.englishText}>
          {selectedItem.sora_name_english}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#707070',
    fontSize: 25,
    fontFamily: 'ElMessiriBold',
    textTransform: 'capitalize',
    lineHeight: 39,
    alignSelf: 'center',
  },
  text: {fontSize: 17, lineHeight: 26},
  englishText: {
    fontFamily: 'ElMessiri-Regular',
    fontSize: 11,
    lineHeight: 17,
    alignSelf: 'center',
    color: '#707070',
  },
  image: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 35,
  },
  soraIcon: {
    width: 70,
    height: 70,
  },
  soraType: {
    width: 30,
    height: 30,
    position: 'absolute',
  },
});

export default connect(TitleComponent);
