import axios from 'axios';
import {getStorage} from '../services/localStorageManager';

const getService = async route => {
  const user = await getStorage('data');
  const userToken = user.token;
  const headers = {
    'Content-Type': 'application/json',
    tokenHeader: `${userToken}`,
  };
  // console.log(userToken, headers);
  const {data} = await axios.get(
    `https://7astc125f5.execute-api.us-east-1.amazonaws.com/Dev_template/${route}`,
    {headers: headers},
  );
  return data;
};

export const getAssetByKey = async (route, Key) => {
  const user = await getStorage('data');
  const userToken = user.token;
  const headers = {
    'Content-Type': 'application/json',
    tokenHeader: `${userToken}`,
  };
  // console.log(userToken, headers);
  const {data} = await axios.get(
    `${'https://7astc125f5.execute-api.us-east-1.amazonaws.com/dev/'}${route}`,
    {
      params: Key,
      headers: headers,
    },
  );
  return data;
};

export default getService;
