import postService from '../axios/postService';
import {getStorage} from './localStorageManager';

export const splitAya = aya => {
  console.log('\n\nAya Recieved', aya);
  try {
    return aya
      .split(' ')
      .filter(i => i)
      .reverse();
  } catch (error) {
    console.log('\n\nError in Aya', error);
  }
};

export const getAyaCorrection = async ({
  sora_id,
  ayah_number,
  user_text,
  audioBase64,
}) => {
  console.log('\n\ngetAyaCorrection invoked');
  const user_id = (await getStorage('data')).user.id;
  const response = await postService('comparetextanduploadmp3', {
    sora_id,
    ayah_number,
    user_id,
    user_text,
    audioBase64,
  });
  if (response.status !== 200) {
    console.log('error');
  }
  console.log('res', response.data);
  return response;
};

export const getWordCorrection = async ({
  sora_id,
  ayah_number,
  user_text,
  audioBase64,
  word_index,
}) => {
  console.log('\n\ngetWordCorrection invoked');
  const user_id = (await getStorage('data')).user.id;
  console.log({
    sora_id,
    ayah_number,
    user_id,
    user_text,
    audioBase64,
    word_index,
  });
  const response = await postService('user/comapre-words', {
    sora_id,
    ayah_number,
    user_id,
    user_text,
    audioBase64,
    word_index,
  });
  if (response.status !== 200) {
    console.log('error');
  }
  console.log('res', response.data);
  return response;
};

export const checkTajweed = async ({ayah_id, hokm_id, base64}) => {
  console.log('tajweed invoked');
  const user_id = (await getStorage('data')).user.id;
  const response = await postService('user/check-tagwed', {
    user_id,
    ayah_id,
    hokm_id,
    base64,
  });
  if (response.status !== 200) {
    console.log('error');
  }
  console.log('res', JSON.stringify(response.data, null, 2));
  return response;
};

export const prepareAyaWords = correctionData => {
  return correctionData.map(word => ({
    word: word.word,
    status: word.status,
    color: word.status === 'true' ? '#00957e' : 'red',
  }));
};

export const check = async correctionData => {
  return correctionData.filter(word => word.status === 'false');
};
