import {Platform, PermissionsAndroid} from 'react-native';
import Permissions from 'react-native-permissions';

async function requestPermission() {
  if (Platform.OS === 'android') {
    try {
      const storage = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Permissions for write access',
          message: 'Give permission to your storage to write a file',
          buttonPositive: 'ok',
        },
      );
      const audio = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        {
          title: 'Permissions for write access',
          message: 'Give permission to your microphone to record voice',
          buttonPositive: 'ok',
        },
      );
      if (
        storage === PermissionsAndroid.RESULTS.GRANTED &&
        audio === PermissionsAndroid.RESULTS.GRANTED
      ) {
        console.log('You can use the storage');
      } else {
        console.log('permission denied');
        return;
      }
    } catch (err) {
      console.warn(err);
      return;
    }
  }
}

export async function checkPermission() {
  const s = await Permissions.check(
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
  );
  const a = await Permissions.check(
    PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
  );
  console.log(`Permission check for storage ${s} and microphone ${a}`);
  if (
    s === PermissionsAndroid.RESULTS.GRANTED &&
    a === PermissionsAndroid.RESULTS.GRANTED
  ) {
    return;
  }
  return requestPermission();
}
