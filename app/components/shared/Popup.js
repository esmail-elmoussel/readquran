import React, {Component} from 'react';
import {StyleSheet, Image} from 'react-native';
import {Text, View, Button, Textarea} from 'native-base';
import Modal from 'react-native-modal';
import ImamButton from './imamButton';
import {ScrollView} from 'react-native-gesture-handler';

class Popup extends Component {
  onChangeText = message => {
    this.props.getMesssage(message);
  };

  render() {
    const {
      visible,
      toggleModal,
      title,
      imageSource,
      text,
      bottomButtonText,
      topButtonText,
      placeholder,
      onBottomButtonPress,
      onTopButtonPress,
      loading,
    } = this.props;

    return (
      <Modal
        isVisible={visible}
        backdropColor={'#C4C1C1'}
        backdropOpacity={1}
        onBackdropPress={toggleModal}
        onBackButtonPress={toggleModal}>
        <View style={styles.container}>
          <ScrollView>
            <View style={styles.itemContainer}>
              <Button onPress={toggleModal} style={styles.exitButton}>
                <Image
                  source={require('../../assets/img/exit.png')}
                  style={styles.exitIcon}
                />
              </Button>
              <Text allowFontScaling={false} style={styles.title}>
                {title}
              </Text>
              <Image source={imageSource} style={styles.image} />
              <Text allowFontScaling={false} style={styles.text}>
                {text}
              </Text>
              {placeholder ? (
                <Textarea
                  onChangeText={message => {
                    this.onChangeText(message);
                  }}
                  style={styles.textArea}
                  rowSpan={3}
                  placeholder={placeholder}
                  placeholderTextColor="#B5B5B5"
                  bordered
                />
              ) : null}
            </View>
            <ImamButton
              disabled={true}
              loading={loading}
              buttonText={topButtonText}
              onPress={onTopButtonPress}
            />
            <Text
              onPress={onBottomButtonPress}
              allowFontScaling={false}
              style={styles.bottomButton}>
              {bottomButtonText}
            </Text>
          </ScrollView>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fbfaf8',
    width: '95%',
    alignSelf: 'center',
    borderRadius: 22,
  },
  itemContainer: {
    alignItems: 'center',
    padding: 20,
  },
  exitButton: {
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 15,
    top: 25,
    width: 24,
    height: 24,
    borderRadius: 100,
  },
  exitIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontFamily: 'ElMessiri-Regular',
    fontSize: 25,
    color: '#707070',
    lineHeight: 40,
    textAlign: 'center',
  },
  image: {
    height: 116,
    resizeMode: 'contain',
    marginVertical: 20,
  },
  text: {
    fontFamily: 'CairoRegular',
    fontSize: 17,
    color: '#707070',
    lineHeight: 33,
    textAlign: 'center',
  },
  textArea: {
    backgroundColor: '#FAFAFA',
    marginTop: 12,
    width: '100%',
    height: 89,
    paddingHorizontal: 20,
    borderRadius: 15,
    borderBottomWidth: 2,
    borderColor: '#E2E2E2',
    color: '#707070',
    fontSize: 13,
    fontFamily: 'CairoRegular',
  },
  bottomButton: {
    alignSelf: 'center',
    fontFamily: 'Cairo-Bold',
    fontSize: 14,
    marginVertical: 10,
    color: '#478D73',
  },
});

export default Popup;
