import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  saturday: false,
  sunday: false,
  monday: false,
  tuesday: false,
  wednesday: false,
  thursday: false,
  friday: false,
};

const reminderReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_SATURDAY_REMINDER:
      return {...state, saturday: action.payload};
    case actionTypes.CHANGE_SUNDAY_REMINDER:
      return {...state, sunday: action.payload};
    case actionTypes.CHANGE_MONDAY_REMINDER:
      return {...state, monday: action.payload};
    case actionTypes.CHANGE_TUESDAY_REMINDER:
      return {...state, tuesday: action.payload};
    case actionTypes.CHANGE_WEDNESDAY_REMINDER:
      return {...state, wednesday: action.payload};
    case actionTypes.CHANGE_THURSDAY_REMINDER:
      return {...state, thursday: action.payload};
    case actionTypes.CHANGE_FRIDAY_REMINDER:
      return {...state, friday: action.payload};
    default:
      return state;
  }
};

export default reminderReducer;
