/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Image} from 'react-native';
import connect from '../connectedComponent/index';
import {Content, View, Text, Button} from 'native-base';
import FullAyaWordComponent from '../components/ayaView/fullAyaWord';
import {Actions} from 'react-native-router-flux';
import Mic from '../components/shared/micButton';
import AudioButton from '../components/ayaView/audioButton';
import {getAyaCorrection, check} from '../services/ayaService';
import PopupMessage from '../components/shared/message';
import {setStorage, getStorage} from '../services/localStorageManager';
import {FlatList} from 'react-native-gesture-handler';
import Layout from '../components/shared/Layout';
import Spinner from 'react-native-loading-spinner-overlay';
import RNFS from 'react-native-fs';
import Share from 'react-native-share';

class ReadByAyaDaily extends React.Component {
  constructor() {
    super();
    this.state = {
      color: 'white',
      Tafsir: '',
      results: '',
      correction: [],
      show: false,
      isRight: false,
      refreshLoading: false,
      showWordHokm: false,
      arabicWord: '',
      arabicWordHokm: [],
      shareDisabled: true,
      user_record_base64: '',
      ayahIndex: 0,
    };
  }

  componentDidMount = async () => {
    await this.setState({ayahIndex: (await getStorage('userCurrentAya')) || 0});
    this.updateAya();
  };

  updateAya = async () => {
    const {ayahs} = this.props;
    const {ayahIndex} = this.state;

    const sora_id = ayahs?.[ayahIndex]?.surah?.number;
    const ayah_id = ayahs?.[ayahIndex]?.numberInSurah;
    const requestData = {
      sora_id,
      ayah_id,
    };
    await this.props?.fetchAyah(requestData);
  };

  getSpeechResult = async data => {
    this.setState({results: data.userText, user_record_base64: data.base64});
    const {results, ayahIndex} = this.state;
    const translate = this.props?.language?.text;
    const {ayahs} = this.props;

    if (!data.userText) {
      alert(translate.No_data_Found_Try_Again);
    } else {
      try {
        const correction = await getAyaCorrection({
          sora_id: ayahs?.[ayahIndex]?.surah?.number,
          ayah_number: ayahs?.[ayahIndex]?.numberInSurah,
          user_text: results,
          audioBase64: data.base64,
        });
        const checkValue = await check(correction.data);
        this.setState({correction: correction.data});
        if (checkValue.length === 0) {
          this.setState({isRight: true, showWordHokm: false});
        } else {
          this.setState({isRight: false});
        }
        this.setState({show: true, shareDisabled: false});
      } catch (error) {
        Actions.noInternet({error: true});
      }
    }
  };

  hideMessage = () => {
    this.setState({show: false, isRight: false});
    Actions.refresh();
  };

  share = () => {
    const {user_record_base64} = this.state;
    const path = `${RNFS.DocumentDirectoryPath}/audio.mp3`;
    RNFS.writeFile(path, user_record_base64, 'base64')
      .then(() => {
        Share.open({
          url: 'file://' + path,
          type: 'audio/mp3',
        });
      })
      .catch(console.log);
  };

  getNextAya = async () => {
    const {ayahIndex} = this.state;
    const {ayahs} = this.props;
    const userCurrentPage = await getStorage('userCurrentPage');

    if (ayahIndex === ayahs[ayahIndex]?.surah?.numberOfAyahs - 1) {
      // User finished his daily read
      setStorage({
        key: 'userCurrentPageDone',
        data: true,
      });
      await setStorage({
        key: 'userCurrentPage',
        data: userCurrentPage + 1,
      });
      setStorage({
        key: 'userCurrentAya',
        data: 0,
      });
      Actions.dailyReadCompleted();
    } else {
      setStorage({
        key: 'userCurrentAya',
        data: ayahIndex + 1,
      });
      await this.setState(state => {
        return {
          ayahIndex: state.ayahIndex + 1,
          refreshLoading: true,
          correction: [],
          show: false,
        };
      });
      await this.updateAya();
      Actions.refresh(this.setState({refreshLoading: false}));
    }
  };

  render() {
    const translate = this.props?.language?.text;
    const {ayahs} = this.props;
    const {
      refreshLoading,
      correction,
      show,
      isRight,
      showWordHokm,
      arabicWord,
      arabicWordHokm,
      shareDisabled,
      ayahIndex,
    } = this.state;

    return (
      <Layout>
        <Content>
          <Text style={styles.title}>{ayahs?.[ayahIndex]?.surah?.name}</Text>
          <Text allowFontScaling={false} style={styles.Text}>
            {translate.Start_reading_from_right_to_left}
          </Text>

          <View style={styles.ayaBody}>
            <FullAyaWordComponent correction={correction} />
          </View>

          {showWordHokm && arabicWord ? (
            <View>
              <Text style={styles.wrongWord}>{arabicWord}</Text>
              {arabicWordHokm ? (
                <FlatList
                  horizontal
                  data={arabicWordHokm}
                  renderItem={({item}) => {
                    return <Text style={styles.wrongWordHokm}>{item}</Text>;
                  }}
                  contentContainerStyle={styles.wrongWordAhkam}
                  keyExtractor={(item, index) => index.toString()}
                />
              ) : null}
            </View>
          ) : null}

          <View style={[styles.buttonView, {display: show ? 'none' : 'flex'}]}>
            <Mic
              getSpeechResult={this.getSpeechResult}
              micPressed={() => this.setState({shareDisabled: true})}
            />
            <AudioButton
              sora={ayahs?.[ayahIndex]?.surah?.number}
              ayah={ayahs?.[ayahIndex]?.numberInSurah}
            />
          </View>
          <PopupMessage
            isRight={isRight}
            show={show}
            onNextPress={this.getNextAya}
            onTryAgainPress={this.hideMessage}
          />
        </Content>
        <Text onPress={() => Actions.quran()} style={styles.skipButton}>
          Skip for now
        </Text>
        <Button
          disabled={shareDisabled}
          style={[
            styles.shareButton,
            shareDisabled && styles.shareButtonDisabled,
          ]}
          onPress={this.share}>
          <Image
            style={styles.shareIcon}
            source={require('../assets/img/icon/share.png')}
          />
        </Button>
        <Spinner visible={refreshLoading} />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#707070',
    fontSize: 25,
    fontFamily: 'ElMessiriBold',
    textTransform: 'capitalize',
    lineHeight: 39,
    alignSelf: 'center',
    marginTop: 10,
  },
  Text: {
    fontSize: 14,
    fontFamily: 'CairoRegular',
    color: '#707070',
    paddingHorizontal: 10,
    lineHeight: 26,
  },
  ayaBody: {
    paddingTop: 10,
    paddingBottom: 25,
    marginTop: 10,
    backgroundColor: '#f7f6f1',
    borderColor: '#C9AD28',
    borderWidth: 1,
    width: '95%',
    alignSelf: 'center',
    borderRadius: 11,
    alignItems: 'center',
    padding: 5,
  },
  wrongWord: {
    fontFamily: 'Amiri',
    fontSize: 30,
    textAlign: 'center',
    color: '#D64944',
  },
  wrongWordHokm: {
    fontFamily: 'CairoRegular',
    fontSize: 14,
    textAlign: 'center',
    color: '#444444',
    marginHorizontal: 5,
  },
  buttonView: {
    height: 150,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  wrongWordAhkam: {
    width: '100%',
    justifyContent: 'center',
  },
  skipButton: {
    position: 'absolute',
    left: 20,
    bottom: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 100,
    padding: 10,
    color: '#707070',
  },
  shareButton: {
    position: 'absolute',
    right: 20,
    bottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    width: 70,
    height: 70,
    alignSelf: 'center',
    borderRadius: 100,
  },
  shareButtonDisabled: {
    opacity: 0.6,
  },
  shareIcon: {
    width: 40,
    height: 40,
  },
});

export default connect(ReadByAyaDaily);
