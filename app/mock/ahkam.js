const shortSurah = [
  {
    id: 5,
    name: 'الادغام',
    englishName: 'Al-Idghaam',
    language: 'الادخال',
    usage:
      'التقاء حرف ساكن بحرف متحرك بحيث يصير الحرفان حرفا واحدا مشددا من جنس الثاني',
    letters: 'له حرف واحد و هو حرف الميم',
  },
  {
    id: 6,
    name: 'الاخفاء',
    englishName: 'Al-Ikhfaa',
    language: 'الستر',
    usage:
      'هو النطق بحرف ساكن عار عن التشديد على صفة بين الاظهار و الادغام مع بقاء الغنة في الحرف الاول و هو هنا الميم الساكنة',
    letters: 'له حرف واحد و هو حرف الباء',
  },
  {
    id: 7,
    name: 'التفخيم',
    englishName: 'Al-Tafkhim',
    language: null,
    usage: 'هو تسمين صوت الحرف عند النطق به فيمتلئ الفم بصدى الحرف',
    letters: ' اللام، وتكون فقط في لفظ الجلالة',
  },
  {
    id: 8,
    name: 'الترقيق',
    englishName: 'Al-Tarqiq',
    language: null,
    usage: 'هو تنحيف صوت الحرف عند النطق به فلا يمتلئ الفم بصدى الحرف',
    letters: 'اللام، في لفظ الجلالة إذا وقعت بعد كسر',
  },
];

export default shortSurah;
