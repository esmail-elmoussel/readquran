/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Button} from 'native-base';
import {StyleSheet, Image} from 'react-native';
import connect from '../../connectedComponent/index';

class Message extends Component {
  messageText = () => {
    const {language, isRight} = this.props;
    const translate = language?.text;
    let message = '';

    if (isRight) {
      message = translate.Right_Recitation;
    } else {
      message = translate.Wrong_Recitation;
    }
    return message;
  };

  messageBody = () => {
    const {isRight, language} = this.props;
    const translate = language?.text;

    let body = '';
    if (isRight) {
      body = translate.Lets_go_to_the_next_one;
    } else {
      body = translate.You_will_do_it_next_time;
    }
    return body;
  };

  render() {
    const translate = this.props.language?.text;
    const {onNextPress, onTryAgainPress} = this.props;

    return (
      <View
        style={[
          styles.message,
          {
            backgroundColor: this.props.isRight ? '#ccddc7' : '#f5cfca',
            display: this.props.show ? 'flex' : 'none',
            height: 200,
            marginTop: 10,
            top: 0,
          },
        ]}>
        <Text
          allowFontScaling={false}
          style={[
            styles.messageText,
            {
              color: this.props.isRight ? '#59AC4F' : '#D64944',
            },
          ]}>
          {this.messageText()}
        </Text>
        <Text
          allowFontScaling={false}
          style={[
            styles.messageBody,
            {
              color: this.props.isRight ? '#59AC4F' : '#D64944',
            },
          ]}>
          {this.messageBody()}
        </Text>
        <View
          style={[
            styles.imageContainer,
            {
              backgroundColor: this.props.isRight ? '#59AC4F' : '#D64944',
              right: 0,
            },
          ]}>
          <Image
            source={require('../../assets/img/message.png')}
            style={styles.image}
          />
        </View>
        <Button
          style={[
            styles.Button,
            {
              backgroundColor: this.props.isRight ? '#59AC4F' : '#D64944',
              borderColor: this.props.isRight ? '#3D7836' : '#A8443C',
            },
          ]}
          onPress={onNextPress}>
          <Text allowFontScaling={false} style={styles.buttonText}>
            {translate.Next}
          </Text>
        </Button>
        <Text
          allowFontScaling={false}
          style={styles.tryAgain}
          onPress={onTryAgainPress}>
          Try Again
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  message: {
    width: '95%',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 11,
    padding: 15,
    marginBottom: 25,
  },
  messageText: {
    fontSize: 21,
    color: 'white',
    lineHeight: 35,
    fontFamily: 'Cairo-Bold',
    marginBottom: 7,
  },
  messageBody: {
    fontSize: 13,
    color: 'white',
    lineHeight: 16,
    fontFamily: 'CairoRegular',
  },
  messageBodyView: {
    flexDirection: 'row',
  },
  Button: {
    width: '90%',
    alignSelf: 'center',
    height: 45,
    borderRadius: 11,
    justifyContent: 'center',
    marginTop: 20,
    borderWidth: 0.5,
    borderBottomWidth: 3,
  },
  buttonText: {
    alignSelf: 'center',
    fontSize: 21,
    textTransform: 'capitalize',
    fontFamily: 'segoe-ui',
    lineHeight: 28,
  },
  imageContainer: {
    width: 40,
    height: 35,
    position: 'absolute',
    top: 35,
    marginHorizontal: 15,
    borderRadius: 5,
    padding: 10,
    justifyContent: 'center',
  },
  image: {
    width: 20,
    height: 20,
    alignSelf: 'center',
  },
  tryAgain: {
    alignSelf: 'center',
    fontSize: 21,
    textTransform: 'capitalize',
    fontFamily: 'segoe-ui',
    marginTop: 10,
  },
});

export default connect(Message);
