import {combineReducers} from 'redux';
import fetchSurahReducer from './fetchSurahReducer';
import fetchAyahReducer from './fetchAyahReducer';
import fetchAhkamReducer from './fetchAhkamReducer';
import updateLanguageReducer from './updateLanguageReducer';
import updateUserDataReducer from './updateUserDataReducer';
import fetchShortSoraQuestionsReducer from './shortSoraQuestionsReducer';
import fetchDailyInfoReducer from './fetchDailyInfoReducer';
import fetchRingsReducer from './fetchRingsReducer';
import fetchSuarReducer from './fetchSuarReducer';
import fetchWrongAyatReducer from './fetchWrongAyatReducer';
import audioButtonReducer from './audioButtonReducer';
import ayaReasonsReducer from './ayaReasonsReducer';
import fetchAyaReasonReducer from './fetchRecitReducer';
import soundsReducer from './soundsReducer';
import fetchProfileImage from './fetchUserImageReducer';
import reminderReducer from './reminderReducer';
// ... other reducers

export default combineReducers({
  fetchSurahReducer,
  fetchAyahReducer,
  fetchAhkamReducer,
  updateLanguageReducer,
  updateUserDataReducer,
  fetchShortSoraQuestionsReducer,
  fetchDailyInfoReducer,
  fetchRingsReducer,
  fetchSuarReducer,
  fetchWrongAyatReducer,
  audioButtonReducer,
  ayaReasonsReducer,
  fetchAyaReasonReducer,
  soundsReducer,
  fetchProfileImage,
  reminderReducer,
  // ... other reducers
});
