import * as actionTypes from './actionTypes';
import {setStorage} from '../services/localStorageManager';

const reminderAction = (day, event) => {
  if (day === 'saturday') {
    setStorage({key: 'saturdayReminder', data: event});
    return dispatch => {
      dispatch({type: actionTypes.CHANGE_SATURDAY_REMINDER, payload: event});
    };
  } else if (day === 'sunday') {
    setStorage({key: 'sundayReminder', data: event});
    return dispatch => {
      dispatch({type: actionTypes.CHANGE_SUNDAY_REMINDER, payload: event});
    };
  } else if (day === 'monday') {
    setStorage({key: 'mondayReminder', data: event});
    return dispatch => {
      dispatch({type: actionTypes.CHANGE_MONDAY_REMINDER, payload: event});
    };
  } else if (day === 'tuesday') {
    setStorage({key: 'tuesdayReminder', data: event});
    return dispatch => {
      dispatch({type: actionTypes.CHANGE_TUESDAY_REMINDER, payload: event});
    };
  } else if (day === 'wednesday') {
    setStorage({key: 'wednesdayReminder', data: event});
    return dispatch => {
      dispatch({type: actionTypes.CHANGE_WEDNESDAY_REMINDER, payload: event});
    };
  } else if (day === 'thursday') {
    setStorage({key: 'thursdayReminder', data: event});
    return dispatch => {
      dispatch({type: actionTypes.CHANGE_THURSDAY_REMINDER, payload: event});
    };
  } else {
    setStorage({key: 'fridayReminder', data: event});
    return dispatch => {
      dispatch({type: actionTypes.CHANGE_FRIDAY_REMINDER, payload: event});
    };
  }
};

export default reminderAction;
